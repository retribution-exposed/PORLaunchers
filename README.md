Pirates Online Retribution Launcher Source Code
=================================================
This repository contains all launcher source code for Pirates Online Retribution.

### Info
* The respository must maintain two folders, "mac" and "windows"
* Both these folders will contain properly labeled sub-directories of launchers based on framework -- containing the launcher and updater dirs. (Eg. mac/electron_1/launcher/src and mac/electron_1/updater/src)
* *Never* force a git push, and always double check with other developers before a merge.
* Create a temporary branch if you are uncertain your changes will have a negative effect to the repository/

### Commits
All commits must start with the resource you are updating following an identifier for your the resource:
````
git pull
git add {$changedFile} 
git commit -m "mac:electron:launcher:main.js Updates to download methods"
git push
````
### Production
**Security**: It is essential that you double check that all developer / debugging tools are disabled, especially logging. This is critically important because we do not want to show the end user things like our client hash, server IP, etc. This could be detrimental to POR's internal security and there will be severe consequences for allowing this to happen

**Documentation**: Each Launcher Developer is required to write documentation and basic QA tips for launcher issues, for each platform. Upon doing so, it must be posted somewhere in discord where end-users can easily access it.
