/*

		Author: Sean Oberoi
		Organization: Pirates Online Retribution
		Product: Pirates Online Retribution Launcher
		Date: 3/10/16
		Copyright 2016 Sean Oberoi and Pirates Online Retribution

*/

function checkInfo() {
	var username = document.getElementById('username-box').value;
	var password = document.getElementById('password-box').value;
	var alertMessageClassName = "alert alert-warning";
	var alertMessageText;
	var formData = "user=" + username + "&pass=" + password;
	if ((username == "") || (password == "") || ((password) == "" && (username == ""))) {
		document.getElementById("password-box").style = "border: 1px ridge #FF0000;";
		document.getElementById("username-box").style = "border: 1px ridge #FF0000;";
	} else {
		var xhttp = new XMLHttpRequest();
		var link = "https://piratesonline.us/_t.php";
		var reqMethod = "POST";
		var async = true;
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4) {
				console.log(xhttp.responseText);
				var parsedData = JSON.parse(xhttp.responseText);
				if (parsedData.status == ("error")) {
					document.getElementById("password-box").style = "border: 1px ridge #FF0000;";
					document.getElementById("username-box").style = "border: 1px ridge #FF0000;";
				} else {
					document.getElementById("password-box").style = "border: 1px ridge #009933;";
					document.getElementById("username-box").style = "border: 1px ridge #009933;";
					getKeyandFields(parsedData);
				}
			}
		}
		xhttp.open(reqMethod, link, async);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(formData);
	}
}

function getKeyandFields(loginDataParse) {
	var $iλ = new XMLHttpRequest();
	var reqProto = "GET";
	var newsLink = "https://raw.githubusercontent.com/POROperations/releases/master/patchmanifest_mac_dev.json";
	var asyncOption = true;
	$iλ.onreadystatechange = function() {
		if (this.readyState == 4) {
			var parsData = JSON.parse(this.responseText);
			processD(parsData, loginDataParse);
			console.log(parsData);
		}
	}
	$iλ.open(reqProto, newsLink, asyncOption);
	$iλ.send();
}

function processD(parsedData, loginDataParse) {
	var data = parsedData;
	var dataLength = parsedData.length;
	var fieldName = Object.keys(data);
	var fieldLength = fieldName.length;
	getUserPath(loginDataParse, parsedData);
}

function getDependencies(loginDataParse) {
	var $iλ = new XMLHttpRequest();
	var reqProto = "GET";
	var newsLink = "https://raw.githubusercontent.com/POROperations/mac-releases/master/dependencies.json";
	var asyncOption = true;
	$iλ.onreadystatechange = function() {
		if (this.readyState == 4) {
			var parsData = JSON.parse(this.responseText);
			processExec(loginDataParse, parsData);
			console.log(parsData);
		}
	}
	$iλ.open(reqProto, newsLink, asyncOption);
	$iλ.send();
}

function processExec(loginDataParse, parsedData) {
	var data = parsedData;
	var dataLength = parsedData.length;
	var fieldName = Object.keys(data);
	var fieldLength = fieldName.length;
	startGame(loginDataParse, parsedData);
}