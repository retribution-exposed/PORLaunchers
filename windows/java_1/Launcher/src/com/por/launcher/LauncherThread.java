package com.por.launcher;

import javax.swing.SwingWorker;

public class LauncherThread extends SwingWorker<Void, Boolean> {

	private Main main;
	
	public LauncherThread(Main main) {
		this.main = main;
	}
	
	public Main getMain() {
		return main;
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		return null;
	}
}