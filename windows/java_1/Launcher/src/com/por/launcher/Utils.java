package com.por.launcher;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.zip.GZIPInputStream;

public class Utils {

	public static String readLink(String link) {
		try {
	        URLConnection connection = new URL(link).openConnection();
	        BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        String result = input.readLine();
	        
	        input.close();
	        return result;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static void copyStreamToFile(InputStream stream, String filename) throws Exception {
		ReadableByteChannel byteChannel = Channels.newChannel(stream);
		FileOutputStream output = new FileOutputStream(filename);
		output.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
		byteChannel.close();
		output.close();
	}
	
	public static void openBrowser(String link) {
		try {
    		Desktop.getDesktop().browse(new URI(link));
		} catch (Exception e) {
			// Not important.
		}
	}
	
	public static void downloadFile(String link, String filename) throws Exception {
		copyStreamToFile(new URL(link).openStream(), filename);
	}
	
	public static void gunzipFile(String filename) throws Exception {
		copyStreamToFile(new GZIPInputStream(new FileInputStream(filename)), filename.replace(".gz", ""));
	}
}