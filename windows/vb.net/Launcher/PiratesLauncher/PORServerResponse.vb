﻿Public Class PORServerResponse
    ' Defines all the variables for the json response
    Public Property status As String
    Public Property message As String
    Public Property token As String
    Public Property ip As String
    Public Property code As String
End Class
