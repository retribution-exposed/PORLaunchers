﻿Imports System.ComponentModel
Imports System.IO
Imports System.Linq
Imports System.Net
Imports System.Security.Cryptography
Imports ICSharpCode.SharpZipLib.Core
Imports ICSharpCode.SharpZipLib.GZip
Imports Newtonsoft.Json

Public Class Launcher
    Private result() As RootObject
    Private Const baseURL As String = "https://raw.githubusercontent.com/POROperations/releases/master/"
    Private CurrentFile As Double
    Private Prog As Integer
    Private TotalFiles As Double
    Private Sub Launcher_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtUser.Text = My.Settings.username
        txtPass.Text = My.Settings.password
        cbSaveLogin.Select()
        ' Start a check for latest news
        News()
    End Sub
#Region "Moveable Form Code"
    Private IsFormBeingDragged As Boolean = False
    Private MouseDownX As Integer
    Private MouseDownY As Integer

    Private Sub Form1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles MyBase.MouseDown

        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = True
            MouseDownX = e.X
            MouseDownY = e.Y
        End If
    End Sub

    Private Sub Form1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs) Handles MyBase.MouseUp

        If e.Button = MouseButtons.Left Then
            IsFormBeingDragged = False
        End If
    End Sub

    Private Sub Form1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs) Handles MyBase.MouseMove

        If IsFormBeingDragged Then
            Dim temp As Point = New Point()

            temp.X = Me.Location.X + (e.X - MouseDownX)
            temp.Y = Me.Location.Y + (e.Y - MouseDownY)
            Me.Location = temp
            temp = Nothing
        End If
    End Sub

#End Region
#Region "Button Behaviors"
#Region "Exit Button"
    Private Sub btnExit_MouseEnter(sender As Object, e As EventArgs) Handles btnExit.MouseEnter
        btnExit.BackgroundImage = My.Resources.TQUIT1D
    End Sub
    Private Sub btnExit_MouseLeave(sender As Object, e As EventArgs) Handles btnExit.MouseLeave
        btnExit.BackgroundImage = My.Resources.TQUIT1U
    End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Hide()
        PlaySoundFile()
        Me.Close()
    End Sub
#End Region
#Region "Minimize Button"
    Private Sub btnMinimize_MouseEnter(sender As Object, e As EventArgs) Handles btnMinimize.MouseEnter
        btnMinimize.BackgroundImage = My.Resources.TMIN1D
    End Sub
    Private Sub btnMinimize_MouseLeave(sender As Object, e As EventArgs) Handles btnMinimize.MouseLeave
        btnMinimize.BackgroundImage = My.Resources.TMIN1U
    End Sub
    Private Sub btnMinimize_Click(sender As Object, e As EventArgs) Handles btnMinimize.Click
        Me.WindowState = FormWindowState.Minimized
        PlaySoundFile()
    End Sub
#End Region
#Region "Play Button"
    Private Sub btnPlay_MouseEnter(sender As Object, e As EventArgs) Handles btnPlay.MouseEnter
        btnPlay.BackgroundImage = My.Resources.PLAY1R
    End Sub
    Private Sub btnPlay_MouseLeave(sender As Object, e As EventArgs) Handles btnPlay.MouseLeave
        btnPlay.BackgroundImage = My.Resources.PLAY1D
    End Sub
    Private Sub btnPlay_Click(sender As Object, e As EventArgs) Handles btnPlay.Click
        btnPlay.BackgroundImage = My.Resources.PLAY1R
        ' Save user & pass
        If cbSaveLogin.Checked = True Then
            My.Settings.username = txtUser.Text
            My.Settings.password = txtPass.Text
        End If
        ColorProgressBar1.Visible = True
        Try
            Updater.RunWorkerAsync()
        Catch ex As Exception
            ' Catch error if background worker is busy
        End Try
        PlaySoundFile()
        btnPlay.Enabled = False

    End Sub
#End Region
#Region "Forgot Button"
    Private Sub btnForgot_MouseEnter(sender As Object, e As EventArgs) Handles btnForgot.MouseEnter
        btnForgot.BackgroundImage = My.Resources.FORGOT1D
    End Sub
    Private Sub btnForgot_MouseLeave(sender As Object, e As EventArgs) Handles btnForgot.MouseLeave
        btnForgot.BackgroundImage = My.Resources.FORGOT1U
    End Sub
    Private Sub btnForgot_Click(sender As Object, e As EventArgs) Handles btnForgot.Click
        PlaySoundFile()
        Process.Start("https://piratesonline.us/password.php")
        ' Prevents a white border appearing around btnForgot
        txtUser.Focus()
    End Sub
#End Region
#Region "Create Button"
    Private Sub btnCreateAccount_MouseEnter(sender As Object, e As EventArgs) Handles btnCreateAccount.MouseEnter
        btnCreateAccount.BackgroundImage = My.Resources.CREATEPW1D
    End Sub
    Private Sub btnCreateAccount_MouseLeave(sender As Object, e As EventArgs) Handles btnCreateAccount.MouseLeave
        btnCreateAccount.BackgroundImage = My.Resources.CREATEPW1U
    End Sub
    Private Sub btnCreateAccount_Click(sender As Object, e As EventArgs) Handles btnCreateAccount.Click
        PlaySoundFile()
        Process.Start("https://piratesonline.us/register")
        ' Prevents a white border appearing around btnCreateAccount
        txtUser.Focus()
    End Sub
#End Region
#Region "Official Site Button"
    Private Sub btnOfficialSite_MouseEnter(sender As Object, e As EventArgs) Handles btnOfficialSite.MouseEnter
        btnOfficialSite.BackgroundImage = My.Resources.WEBSITE1R
    End Sub
    Private Sub btnOfficialSite_MouseLeave(sender As Object, e As EventArgs) Handles btnOfficialSite.MouseLeave
        btnOfficialSite.BackgroundImage = My.Resources.WEBSITE1D
    End Sub
    Private Sub btnOfficialSite_Click(sender As Object, e As EventArgs) Handles btnOfficialSite.Click
        PlaySoundFile()
        Process.Start("http://piratesonline.us/")
        ' Prevents a white border appearing around btnOfficialSite
        txtUser.Focus()
    End Sub
#End Region
#Region "Community Button"
    Private Sub btnCommunity_MouseEnter(sender As Object, e As EventArgs) Handles btnCommunity.MouseEnter
        btnCommunity.BackgroundImage = My.Resources.COMMUNITY1R
    End Sub
    Private Sub btnCommunity_MouseLeave(sender As Object, e As EventArgs) Handles btnCommunity.MouseLeave
        btnCommunity.BackgroundImage = My.Resources.COMMUNITY1D
    End Sub
    Private Sub btnCommunity_Click(sender As Object, e As EventArgs) Handles btnCommunity.Click
        PlaySoundFile()
        Process.Start("https://piratesforums.com")
        ' Prevents a white border appearing around btnCommunityClick
        txtUser.Focus()
    End Sub
#End Region
#Region "Players Guide Button"
    Private Sub btnPlayersGuide_MouseEnter(sender As Object, e As EventArgs) Handles btnPlayersGuide.MouseEnter
        btnPlayersGuide.BackgroundImage = My.Resources.FAQ1R
    End Sub
    Private Sub btnPlayersGuide_MouseLeave(sender As Object, e As EventArgs) Handles btnPlayersGuide.MouseLeave
        btnPlayersGuide.BackgroundImage = My.Resources.FAQ1D
    End Sub
    Private Sub btnPlayersGuide_Click(sender As Object, e As EventArgs) Handles btnPlayersGuide.Click
        PlaySoundFile()
        Process.Start("http://piratesonline.us/faq.php")
        ' Prevents a white border appearing around btnPlayersGuide
        txtUser.Focus()
    End Sub
#End Region
#Region "Release Notes Button"
    Private Sub btnReleaseNotes_MouseEnter(sender As Object, e As EventArgs) Handles btnReleaseNotes.MouseEnter
        btnReleaseNotes.BackgroundImage = My.Resources.HOMEPAGE1R
    End Sub
    Private Sub btnReleaseNotes_MouseLeave(sender As Object, e As EventArgs) Handles btnReleaseNotes.MouseLeave
        btnReleaseNotes.BackgroundImage = My.Resources.HOMEPAGE1D
    End Sub
    Private Sub btnReleaseNotes_Click(sender As Object, e As EventArgs) Handles btnReleaseNotes.Click
        PlaySoundFile()
        Process.Start("http://piratesonline.us/news.php")
        ' Prevents a white border appearing around btnReleaseNotes
        txtUser.Focus()
    End Sub
#End Region
#Region "Manage Account Button"
    Private Sub btnManageAccount_MouseEnter(sender As Object, e As EventArgs) Handles btnManageAccount.MouseEnter
        btnManageAccount.BackgroundImage = My.Resources.MANAGEACCOUNT1R
    End Sub
    Private Sub btnManageAccount_MouseLeave(sender As Object, e As EventArgs) Handles btnManageAccount.MouseLeave
        btnManageAccount.BackgroundImage = My.Resources.MANAGEACCOUNT1D
    End Sub
    Private Sub btnManageAccount_Click(sender As Object, e As EventArgs) Handles btnManageAccount.Click
        PlaySoundFile()
        Process.Start("https://piratesonline.us/account.php")
        ' Prevents a white border appearing around btnManageAccount
        txtUser.Focus()
    End Sub
#End Region
#Region "Main Manage Account Button"
    Private Sub btnManageAccDefault_Click(sender As Object, e As EventArgs) Handles btnManageAccDefault.Click
        PlaySoundFile()
        Process.Start("https://piratesonline.us/account.php")
        ' Prevents a white border appearing around btnManageAccount
        txtUser.Focus()
    End Sub
    Private Sub btnManageAccDefault_MouseEnter(sender As Object, e As EventArgs) Handles btnManageAccDefault.MouseEnter
        btnManageAccDefault.BackgroundImage = My.Resources.manage_over
    End Sub
    Private Sub btnManageAccDefault_MouseLeave(sender As Object, e As EventArgs) Handles btnManageAccDefault.MouseLeave
        btnManageAccDefault.BackgroundImage = My.Resources.manage_default
    End Sub
#End Region
#Region "Help Button"
    Private Sub btnHelp_MouseEnter(sender As Object, e As EventArgs) Handles btnHelp.MouseEnter
        btnHelp.BackgroundImage = My.Resources.REPORTBUG1R
    End Sub
    Private Sub btnHelp_MouseLeave(sender As Object, e As EventArgs) Handles btnHelp.MouseLeave
        btnHelp.BackgroundImage = My.Resources.REPORTBUG1D
    End Sub
    Private Sub btnHelp_Click(sender As Object, e As EventArgs) Handles btnHelp.Click
        PlaySoundFile()
        Process.Start("https://piratesonline.us/contact")
        ' Prevents a white border appearing around btnHelp
        txtUser.Focus()
    End Sub
#End Region
#Region "Play on enter"
    Private Sub txtUser_KeyDown(sender As Object, e As KeyEventArgs) Handles txtUser.KeyDown
        If e.KeyCode.Equals(Keys.Enter) Then
            btnPlay.PerformClick()
            e.Handled = True
            e.SuppressKeyPress = True
        End If
    End Sub
    Private Sub txtPass_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPass.KeyDown
        ' Allows user to press enter instead of clicking play
        If e.KeyCode.Equals(Keys.Enter) Then
            btnPlay.PerformClick()
            e.Handled = True
            e.SuppressKeyPress = True
        End If
    End Sub
#End Region
#Region "Change color on re-try of user/pass enter"
    Private Sub txtUser_TextChanged(sender As Object, e As EventArgs) Handles txtUser.TextChanged
        txtUser.ForeColor = Color.Black
    End Sub

    Private Sub txtPass_TextChanged(sender As Object, e As EventArgs) Handles txtPass.TextChanged
        txtPass.ForeColor = Color.Black
    End Sub
#End Region
#Region "Control-A Support"
    Private Sub txtUser_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtUser.KeyPress
        If e.KeyChar = Convert.ToChar(1) Then
            DirectCast(sender, TextBox).SelectAll()
            e.Handled = True
        End If
    End Sub
    Private Sub txtPass_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) Handles txtPass.KeyPress
        If e.KeyChar = Convert.ToChar(1) Then
            DirectCast(sender, TextBox).SelectAll()
            e.Handled = True
        End If
    End Sub

#End Region
#End Region
    Private Sub PlaySoundFile()
        My.Computer.Audio.Play(My.Resources.SNDCLICK,
        AudioPlayMode.WaitToComplete)
    End Sub
    Private Sub Updater_DoWork(sender As Object, e As DoWorkEventArgs) Handles Updater.DoWork
        ' New updater fix
        Dim proc = Process.GetProcessesByName("PORUpdater")
        Try
            proc(0).Kill()
        Catch ex As Exception

        End Try
        '
        Try
            Dim request As HttpWebRequest = WebRequest.Create("https://raw.githubusercontent.com/POROperations/releases/master/patchmanifest_win_dev.json")
            request.Method = "GET"
            Dim response As WebResponse = request.GetResponse()
            Dim dataStream As Stream
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            reader.Close()
            dataStream.Close()
            response.Close()
            Dim status As Dictionary(Of String, String) = JsonConvert.DeserializeObject(Of Dictionary(Of String, String))(responseFromServer)
            ' Turning files into arrays
            Dim files As String() = New String(status.Count - 1) {}
            Dim hashes As String() = New String(status.Count - 1) {}
            status.Values.CopyTo(hashes, 0)
            status.Keys.CopyTo(files, 0)
            Dim Client As New WebClient
            Try
                Dim strPath As String = "\resources\default\"
                If Directory.Exists(Directory.GetCurrentDirectory & strPath) Then
                    Console.WriteLine(Directory.GetCurrentDirectory & "{0} already exists.", strPath)
                Else
                    Console.WriteLine(Directory.GetCurrentDirectory & "{0} doesn't exist! Creating...", strPath)
                    Directory.CreateDirectory(Directory.GetCurrentDirectory & strPath)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString())
            End Try
            Updater.ReportProgress(1 / files.Count)
            For r = 0 To files.Count - 1
                CurrentFile = r + 1
                TotalFiles = files.Count
                Prog = Int((CurrentFile / TotalFiles) * 100)

                If CompareMD5(files(r), hashes(r)) = True Then
                    Updater.ReportProgress(Prog)
                ElseIf files(r).EndsWith("mf.gz") = True Then
                    Dim fileName As String = Replace(files(r), ".gz", "")
                    If FileSize(fileName, hashes(r)) = True Then
                        Updater.ReportProgress(Prog)
                    Else
                        Client.DownloadFile(baseURL & files(r), files(r))
                        ExtractGZip(files(r), Directory.GetCurrentDirectory & "/resources/default/")
                        File.Delete(files(r))
                        Updater.ReportProgress(Prog)
                    End If
                ElseIf files(r) = "PORLauncher.exe" Then
                    Updater.ReportProgress(Prog)
                Else
                    Client.DownloadFile(baseURL & files(r), files(r))
                    Updater.ReportProgress(Prog)
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex, "Error")
        End Try
    End Sub
    Public Function CompareMD5(ByVal filePath As String, ByVal hash As String)
        Dim md As String
        Try
            Using _md5 = MD5.Create()
                Using stream = File.OpenRead(filePath)
                    md = BitConverter.ToString(_md5.ComputeHash(stream)).Replace("-", "").ToLower()
                End Using
            End Using
        Catch ex As Exception
            ' This means the file does not exist on our drive
            Return False
        End Try

        If hash = md Then
            ' Hash is the same
            Return True
        Else
            ' Hash is different
            Return False
        End If
    End Function
    Public Function FileSize(ByVal filePath As String, ByVal size As Integer)
        Try
            Dim myFile As New FileInfo(filePath)
            Dim sizeInBytes As Long = myFile.Length
            If sizeInBytes = size Then
                ' True as in the file is up to date
                Return True
            Else
                ' False as in the file is a different size, update it.
                Return False
            End If
        Catch ex As Exception
            ' The file doesn't exist
            Console.WriteLine("{0} does not exist!", filePath)
            Return False
        End Try
    End Function
    Public Sub ExtractGZip(gzipFileName As String, targetDir As String)
        ' Use a 4K buffer. Any larger is a waste.  
        Try
            Dim dataBuffer As Byte() = New Byte(4095) {}
            Using fs As Stream = New FileStream(gzipFileName, FileMode.Open, FileAccess.Read)
                Using gzipStream As New GZipInputStream(fs)
                    Dim fnOut As String = Path.Combine(targetDir, Path.GetFileNameWithoutExtension(gzipFileName))
                    Using fsOut As FileStream = File.Create(fnOut)
                        StreamUtils.Copy(gzipStream, fsOut, dataBuffer)
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show("An error has occured extracting game files, please restart your launcher.")
        End Try
    End Sub
    Private Sub Updater_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles Updater.ProgressChanged
        ColorProgressBar1.Value = e.ProgressPercentage
        ColorProgressBar1.Update()

        lblUpdate.Text = "Updating: File " + Convert.ToString(CurrentFile) + "/" + Convert.ToString(TotalFiles)
        ' We are waiting for all of our files to update
        If ColorProgressBar1.Value = 100 Then
            Play.getToken(txtUser.Text, txtPass.Text)
            btnPlay.Enabled = True
            lblUpdate.Text = "Finished!"
            txtUser.Focus() ' Prevents white highlighting on certain controls
        ElseIf ColorProgressBar1.Value < 100 Then
            btnPlay.Enabled = False
            ' Change background, move controls, watch video
        End If
    End Sub
#Region "News code"
    Sub News()
        Try
            Dim request As HttpWebRequest = WebRequest.Create("https://piratesonline.us/devtoolkit/_na.php")
            request.Method = "POST"
            Dim response As WebResponse = request.GetResponse()
            Dim dataStream As Stream
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            reader.Close()
            dataStream.Close()
            response.Close()
            result = JsonConvert.DeserializeObject(Of RootObject())(responseFromServer)
            If IfNewsExists() = True Then
                Console.WriteLine("News exists. Updating news...")
                ' We remove the current news so we can append more information to the HTML file
                File.Delete(Path.GetTempPath & "news\index.html")
                CreateHTML()
            Else
                ' Some or all of the files don't exist, lets create the news directory
                Console.WriteLine("News does not exist, creating news")
                Directory.CreateDirectory(Path.GetTempPath & "news\")
                ' Now HTML will be created
                CreateHTML()
            End If

        Catch ex As Exception
            MessageBox.Show("Failed to retrieve latest news. Please make sure your internet connection is working properly.")
        End Try
    End Sub
    Sub CreateHTML()
        Dim f As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(Path.GetTempPath & "news\index.html", True)
        f.WriteLine("<!DOCTYPE HTML>")
        f.WriteLine("<HEAD>
        </HEAD>")
        f.WriteLine("<STYLE>
body {
	font-family:'Georgia';
	margin: 1px;
	background-image: url('data:image/jpeg;base64,/9j/4Q5qRXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAeAAAAcgEyAAIAAAAUAAAAkIdpAAQAAAABAAAApAAAANAADqZ4AAAnEAAOpngAACcQQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykAMjAxNjoxMDoyNCAyMToyNDo0NAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAABuqADAAQAAAABAAABXQAAAAAAAAAGAQMAAwAAAAEABgAAARoABQAAAAEAAAEeARsABQAAAAEAAAEmASgAAwAAAAEAAgAAAgEABAAAAAEAAAEuAgIABAAAAAEAAA00AAAAAAAAAEgAAAABAAAASAAAAAH/2P/tAAxBZG9iZV9DTQAB/+4ADkFkb2JlAGSAAAAAAf/bAIQADAgICAkIDAkJDBELCgsRFQ8MDA8VGBMTFRMTGBEMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAENCwsNDg0QDg4QFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAfgCgAwEiAAIRAQMRAf/dAAQACv/EAT8AAAEFAQEBAQEBAAAAAAAAAAMAAQIEBQYHCAkKCwEAAQUBAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAABBAEDAgQCBQcGCAUDDDMBAAIRAwQhEjEFQVFhEyJxgTIGFJGhsUIjJBVSwWIzNHKC0UMHJZJT8OHxY3M1FqKygyZEk1RkRcKjdDYX0lXiZfKzhMPTdePzRieUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9jdHV2d3h5ent8fX5/cRAAICAQIEBAMEBQYHBwYFNQEAAhEDITESBEFRYXEiEwUygZEUobFCI8FS0fAzJGLhcoKSQ1MVY3M08SUGFqKygwcmNcLSRJNUoxdkRVU2dGXi8rOEw9N14/NGlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vYnN0dXZ3eHl6e3x//aAAwDAQACEQMRAD8A5jr2XiCtrsgjJtmx1DWN9Jm8lou9Zu2vdVXtqZ6bN/rXUfovSoXMlw7k/wCfp962eutDL62NLGAOtA00Gtfufp9F30mfyFlEOaYI544/761MiKDIjL9QdwntLidP+kpsvA9rnbmg7gC6BJ9p3QN30f3VEtedZ58RKRa5updHykSnK1WseDMEaiCQTCm2xgIG4FoPGsf9U1BPm7UnUIjXPDdHAA6TGqSLU+wGddzj+dMlRL4AAMHQd+yi+TpM/wCvxTtc4GQRPOspKtk60bt2nw4TSJ+kBOp10lRLnc+HknBsdpM+OiSrUS2JkRxCQ2/vARwZ800u7RA7R8yFODAkiD5JKYuLZ0++Z/zU42z9IDynSIScHAgSD46cJ9rpgnXtp4pKWJbAg6eBPb+yl7SfpCT+dP5U5BBA3DXuFEh455MgacpKWls9h8SpNA/tDVJoc48x34TjY4zEffCSlEt5mDA76eXPuUZBGrgedCSpRoXNIHg3n8qYh0cy2JlJT//Q4rr1bmGk7wSfUnaCCJNZ2/2pWQZg8x4RpK2OvuHrVn2sO+4kAO7vZ/ncfmLGcQOHCPDVMjsGRQ00BGviE7tBETt4MAafcogmOWnzlTDtDq3705CMkaQPl4pATp+aNSpEiZkSfPT8ijqYAIiPGAkpUCQImI3J9Wu4I7/JRMeMDgT4KUtHBHlyRP3JKUNSREd0wlpOvl5p593I1PiE0CCTGnAlJTIBzSCBM8x3lPDhw0g/SGn8VAOjk/LT/vyk18OJET5HUfikpk8u3kQSD28EmBwIEERoRxyo7zOpB8BpH9nVJr4aRprz3/zpKSbVBJIHuLhr2TQ506THISLtJJBLhrJEpjA4Ijuf9iSGY3mCWklpklRDSQPLnj7wlM6aHxnRLWNd3cgkeKSly7tHHH+rUxnUT4kmE24jUF2p0PmmnSCTz4d/FJVv/9HjPrGJuq3yQDaWyd3Lm+SyjXWNHN2tPBIgjTh0O/srU66IvYXOcDNvuM6kPH/R3exZEtB+PIG6PuTI7Mmiza6yCYE86n/voThtcQRPiRpChvEmT/r+amLwBpz8U5VhdzWc7T80gxh1+j3MhNZa10ANLQ0QIOvzMfvJ2mojkyRPz+SSNFg0H2ga/BL2xEAa8/3ypD0tde8ax/Cf+ilvbudrA1gEzP3JKYw3ynnWFKK/v+HP4JtzYIkQl6kxB+86pK0VtEeenEd1NrGkGWiY08Y8dqgHtHcgx2Kf1BBG4yTJM6ykrRRZr/u/IpbG7dAA6dSNY1/6KgLBptc4HieI8uU5sbt2zoNPFJWjLYdrSNYMcaD+S4pFgLG7QOIlvcxKiXtIjcTJ1nTT+VzuTGxhgkmNBpzokrRO2t+3cJLI1eeNEMEhstkGSJ18P9qVdogje5oLddJnwb/6kSJEAN5Pcaaxzu/O3JJYyB5948/wUREwe8x2hOSCAB8SY1SDg7kcd/h4pIf/0uM69sbkUFsAbXuMubE7tfaPb+b/AJ6yXe0ggRHBP+5aXW7bRbRta0AMdERxu+jZ/wAJv+ksp9tpO5zRr30HwTY7MlsQT8+CSpB+2Y0B799FDc+OD/uSLncbRr4TKKLUfoxw1LXx5GsKJJ10jxHZLc+IA0P5Qki2YcWngOn4pgAR5ePdQL3J5cRxwkq12gGYHwTiCCBwDMqLQ4kjbKW5zdSI8j5eSSmREHuU4j26aHnxUdxJ4A8DKQcSIjnzSUycIg7eRPkUuDECAPxUS4wNAC0H56+SRJIgDgeKSbSObG1pbB00I7Hum1kiAToAovc53u2iNB+CbcQZgaR+B80lWldLXGW6jRwIjXw3KO0tfAJMHkAD8qY7vpQNTp81PcZkDQRqOf8Azp7UlMQwl20uLdSD8Pg1IMJdGp+7t3hT3e52gYBMhp/8kXJNc7dO0c6gHTzbt3bnMSTQf//T4j6wMcLMfdDXNY9pg92uFbpjasja2eZ85/uK2eu1lj62w4Fnq7WmTIDm7tsgfnB9j3LHO9rROnl4psdgvWhmkgz8+EoZpumO/P8Aekdfh4pw8D+On4IqRkNjgptvxgqbd0SIMdim1IkyPxSQtA+SQDBz/qU8x4z2SjXkj4+SSmTWtDSSOdIgqECNRH3p4+AHxTHx/wBdElL7W+ED5pw3tGsdym450P4ypQ4aAETpASSw2zI7qW2QfLkd+Ew1Md54UjuAgAkcxH5UlMQJDYHCct9sxxM+Mp2tfAcQC06iI/1b/aTO3BvcgePASUoCTx4JwwEg7j5f6/1kx3N0dDpEgiO/wTscwbpg+0x/rLUlLRHLjp2TitpJ11gaJg4DcJ+PPPina9uxw0kxGnH4pK0f/9Ti/rE1/qUkkxNse0D85jd39v6W5YpEAERB0IjUHzW39YpFrHHVzX3l0/1qx+8Vjte1u5rhukQCNIPyTY7L0ceXfuD/AHpbRE7eYLeUjtJM+3Qx5x+amkng8CJnt4aoqXDBtJLTHj2lNAgac9/BTJcWwTo46gRE/wDRTEhzjuAbPMf7ElMdOTr8E3Bn+KcxPaOyQIGv4JIUfAcdvApw0QD7fMF0H8qYEd9QZ4/1Kk1zSf3TPy+EQ5JS81wQGw7tB00+PuQz4+J79/xTlvu0PwPf+CRaYmdDxKSlwYZ/KBMEaacKJH+7/Up9up1+JGqRYRGvnqeySlfHU6CE0amIkfBOAO5geASLYMEgx5pKXa0O8CfFMPpDiPh+VOxxaHDeRIPA1J42yk8iQSIjsklYaknxPmk0wZPh4KQ7GJ1nwKiXDdPc+CSn/9XjvrSa/tLNJa23JaBvE6WNDT/237f5awTWOwMeZC3PrKQMlgaRta++HD3GA9v9X+y79xYziJkEDw5n4D+qmx2X0jLPjPxEJbY7Ge0oneCAZPB791F23y18P4FFVMNmukxMDUJ9ABtmTynO0gwTPaOEwJH0TqdNOUkMQ3UQkWEnv4a9lIzAcfhMBMYBO3jzRVS2zy1+IhOBr3B7KQEjxjWE8fyoBOpEcjw/OQVTCAWjkuOk6QnDdZgnkjzTmCSS7U8lIx8o7jVJSxaPPz1SDYnQyI5jxT+34eQSJE68TpKSlizyPlr3KQGnHx18vgnEnQDzgd00iJ7+EeKSlbBP5Ne3kk10OBAE/D8u5OHmOAfPvxCgD/r3SUzLiew+AHz8UmvIJLdCRHAiP6qYl3hz37pgTpH4c6JKf//W5rr+NhX075fh277ThtuEMsBLbLMdxLWPqe6a76X/AM1X6vo3bPV3rlodt3bTA7jRdZ9ZMfDtNovy2499eTkGktr3ssrca9zHel76vQ2t9H2fpmWfov0K57Kw66NcfOqy9JIqbc0gzt2/pqK/zf0n0/8AwT2JkNtV7U9xP0SkQdpJadogEkaT9JL9JpxH5v8ABGo+07XBm2J95dx/Jnf7P6qcpr6azp8U7muY7a4e7uNCnsA1MiT9JsGB8FK4viHBgeHHeR9Iuj87VJCMzEkHUqQdOgGnBMT/ABTe/YCT34MqVYEaugfnSB/0d/5ySVi4kyAdR8Pmm1iSDEHVEf6gd7TIM7Z7ae76PsUB6nlE6pKWeCCQ4FrhoR3EJw0kEjhuruNPvR7vWLf0+1rSdP3hp7e/qfR/eQWS0u+i6sAb5mCJ9vdrklMAZjSNU5B27+W8TpzCkdd2+G+4wWidfkfop6y8DUMIJO3dwHR7okt/8gkpgATwD4+STXeUk/69ikwOnQ6R5/ik0GdT906/ekhlu7mQeQYjU6KBmOQe51HgjEDbyJHOjIJj+SUMfh5pJLAOeJ1AnnXVOZ8ZjTnz7J/HSPy/JPrJn58Qkh//2f/tFwxQaG90b3Nob3AgMy4wADhCSU0EJQAAAAAAEAAAAAAAAAAAAAAAAAAAAAA4QklNBDoAAAAAAZUAAAAQAAAAAQAAAAAAC3ByaW50T3V0cHV0AAAABwAAAABDbHJTZW51bQAAAABDbHJTAAAAAFJHQkMAAAAATm0gIFRFWFQAAAAkAEMAYQBuAG8AbgAgAEkASgAgAEMAbwBsAG8AcgAgAFAAcgBpAG4AdABlAHIAIABQAHIAbwBmAGkAbABlACAAMgAwADAANQAAAAAAAEludGVlbnVtAAAAAEludGUAAAAAQ2xybQAAAABNcEJsYm9vbAEAAAAPcHJpbnRTaXh0ZWVuQml0Ym9vbAAAAAALcHJpbnRlck5hbWVURVhUAAAAHwBDAGEAbgBvAG4AIABNAEcAMwA1ADAAMAAgAHMAZQByAGkAZQBzACAAUAByAGkAbgB0AGUAcgAgAFcAUwAAAAAAD3ByaW50UHJvb2ZTZXR1cE9iamMAAAAMAFAAcgBvAG8AZgAgAFMAZQB0AHUAcAAAAAAACnByb29mU2V0dXAAAAABAAAAAEJsdG5lbnVtAAAADGJ1aWx0aW5Qcm9vZgAAAAlwcm9vZkNNWUsAOEJJTQQ7AAAAAAItAAAAEAAAAAEAAAAAABJwcmludE91dHB1dE9wdGlvbnMAAAAXAAAAAENwdG5ib29sAAAAAABDbGJyYm9vbAAAAAAAUmdzTWJvb2wAAAAAAENybkNib29sAAAAAABDbnRDYm9vbAAAAAAATGJsc2Jvb2wAAAAAAE5ndHZib29sAAAAAABFbWxEYm9vbAAAAAAASW50cmJvb2wAAAAAAEJja2dPYmpjAAAAAQAAAAAAAFJHQkMAAAADAAAAAFJkICBkb3ViQG/gAAAAAAAAAAAAR3JuIGRvdWJAb+AAAAAAAAAAAABCbCAgZG91YkBv4AAAAAAAAAAAAEJyZFRVbnRGI1JsdAAAAAAAAAAAAAAAAEJsZCBVbnRGI1JsdAAAAAAAAAAAAAAAAFJzbHRVbnRGI1B4bEBYAMSAAAAAAAAACnZlY3RvckRhdGFib29sAQAAAABQZ1BzZW51bQAAAABQZ1BzAAAAAFBnUEMAAAAATGVmdFVudEYjUmx0AAAAAAAAAAAAAAAAVG9wIFVudEYjUmx0AAAAAAAAAAAAAAAAU2NsIFVudEYjUHJjQFkAAAAAAAAAAAAQY3JvcFdoZW5QcmludGluZ2Jvb2wAAAAADmNyb3BSZWN0Qm90dG9tbG9uZwAAAAAAAAAMY3JvcFJlY3RMZWZ0bG9uZwAAAAAAAAANY3JvcFJlY3RSaWdodGxvbmcAAAAAAAAAC2Nyb3BSZWN0VG9wbG9uZwAAAAAAOEJJTQPtAAAAAAAQAGADEgABAAIAYAMSAAEAAjhCSU0EJgAAAAAADgAAAAAAAAAAAAA/gAAAOEJJTQQNAAAAAAAEAAAAHjhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAjhCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAAAAAAAAAIAADhCSU0EAgAAAAAAAgAAOEJJTQQwAAAAAAABAQA4QklNBC0AAAAAAAYAAQAAAAM4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADQwAAAAYAAAAAAAAAAAAAAV0AAAG6AAAABwBiAGcAbgBlAHcAcwBmAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAG6AAABXQAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAABAAAAABAAAAAAAAbnVsbAAAAAIAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAABXQAAAABSZ2h0bG9uZwAAAboAAAAGc2xpY2VzVmxMcwAAAAFPYmpjAAAAAQAAAAAABXNsaWNlAAAAEgAAAAdzbGljZUlEbG9uZwAAAAAAAAAHZ3JvdXBJRGxvbmcAAAAAAAAABm9yaWdpbmVudW0AAAAMRVNsaWNlT3JpZ2luAAAADWF1dG9HZW5lcmF0ZWQAAAAAVHlwZWVudW0AAAAKRVNsaWNlVHlwZQAAAABJbWcgAAAABmJvdW5kc09iamMAAAABAAAAAAAAUmN0MQAAAAQAAAAAVG9wIGxvbmcAAAAAAAAAAExlZnRsb25nAAAAAAAAAABCdG9tbG9uZwAAAV0AAAAAUmdodGxvbmcAAAG6AAAAA3VybFRFWFQAAAABAAAAAAAAbnVsbFRFWFQAAAABAAAAAAAATXNnZVRFWFQAAAABAAAAAAAGYWx0VGFnVEVYVAAAAAEAAAAAAA5jZWxsVGV4dElzSFRNTGJvb2wBAAAACGNlbGxUZXh0VEVYVAAAAAEAAAAAAAlob3J6QWxpZ25lbnVtAAAAD0VTbGljZUhvcnpBbGlnbgAAAAdkZWZhdWx0AAAACXZlcnRBbGlnbmVudW0AAAAPRVNsaWNlVmVydEFsaWduAAAAB2RlZmF1bHQAAAALYmdDb2xvclR5cGVlbnVtAAAAEUVTbGljZUJHQ29sb3JUeXBlAAAAAE5vbmUAAAAJdG9wT3V0c2V0bG9uZwAAAAAAAAAKbGVmdE91dHNldGxvbmcAAAAAAAAADGJvdHRvbU91dHNldGxvbmcAAAAAAAAAC3JpZ2h0T3V0c2V0bG9uZwAAAAAAOEJJTQQoAAAAAAAMAAAAAj/wAAAAAAAAOEJJTQQUAAAAAAAEAAAAAzhCSU0EDAAAAAANUAAAAAEAAACgAAAAfgAAAeAAAOxAAAANNAAYAAH/2P/tAAxBZG9iZV9DTQAB/+4ADkFkb2JlAGSAAAAAAf/bAIQADAgICAkIDAkJDBELCgsRFQ8MDA8VGBMTFRMTGBEMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAENCwsNDg0QDg4QFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAfgCgAwEiAAIRAQMRAf/dAAQACv/EAT8AAAEFAQEBAQEBAAAAAAAAAAMAAQIEBQYHCAkKCwEAAQUBAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAABBAEDAgQCBQcGCAUDDDMBAAIRAwQhEjEFQVFhEyJxgTIGFJGhsUIjJBVSwWIzNHKC0UMHJZJT8OHxY3M1FqKygyZEk1RkRcKjdDYX0lXiZfKzhMPTdePzRieUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9jdHV2d3h5ent8fX5/cRAAICAQIEBAMEBQYHBwYFNQEAAhEDITESBEFRYXEiEwUygZEUobFCI8FS0fAzJGLhcoKSQ1MVY3M08SUGFqKygwcmNcLSRJNUoxdkRVU2dGXi8rOEw9N14/NGlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vYnN0dXZ3eHl6e3x//aAAwDAQACEQMRAD8A5jr2XiCtrsgjJtmx1DWN9Jm8lou9Zu2vdVXtqZ6bN/rXUfovSoXMlw7k/wCfp962eutDL62NLGAOtA00Gtfufp9F30mfyFlEOaYI544/761MiKDIjL9QdwntLidP+kpsvA9rnbmg7gC6BJ9p3QN30f3VEtedZ58RKRa5updHykSnK1WseDMEaiCQTCm2xgIG4FoPGsf9U1BPm7UnUIjXPDdHAA6TGqSLU+wGddzj+dMlRL4AAMHQd+yi+TpM/wCvxTtc4GQRPOspKtk60bt2nw4TSJ+kBOp10lRLnc+HknBsdpM+OiSrUS2JkRxCQ2/vARwZ800u7RA7R8yFODAkiD5JKYuLZ0++Z/zU42z9IDynSIScHAgSD46cJ9rpgnXtp4pKWJbAg6eBPb+yl7SfpCT+dP5U5BBA3DXuFEh455MgacpKWls9h8SpNA/tDVJoc48x34TjY4zEffCSlEt5mDA76eXPuUZBGrgedCSpRoXNIHg3n8qYh0cy2JlJT//Q4rr1bmGk7wSfUnaCCJNZ2/2pWQZg8x4RpK2OvuHrVn2sO+4kAO7vZ/ncfmLGcQOHCPDVMjsGRQ00BGviE7tBETt4MAafcogmOWnzlTDtDq3705CMkaQPl4pATp+aNSpEiZkSfPT8ijqYAIiPGAkpUCQImI3J9Wu4I7/JRMeMDgT4KUtHBHlyRP3JKUNSREd0wlpOvl5p593I1PiE0CCTGnAlJTIBzSCBM8x3lPDhw0g/SGn8VAOjk/LT/vyk18OJET5HUfikpk8u3kQSD28EmBwIEERoRxyo7zOpB8BpH9nVJr4aRprz3/zpKSbVBJIHuLhr2TQ506THISLtJJBLhrJEpjA4Ijuf9iSGY3mCWklpklRDSQPLnj7wlM6aHxnRLWNd3cgkeKSly7tHHH+rUxnUT4kmE24jUF2p0PmmnSCTz4d/FJVv/9HjPrGJuq3yQDaWyd3Lm+SyjXWNHN2tPBIgjTh0O/srU66IvYXOcDNvuM6kPH/R3exZEtB+PIG6PuTI7Mmiza6yCYE86n/voThtcQRPiRpChvEmT/r+amLwBpz8U5VhdzWc7T80gxh1+j3MhNZa10ANLQ0QIOvzMfvJ2mojkyRPz+SSNFg0H2ga/BL2xEAa8/3ypD0tde8ax/Cf+ilvbudrA1gEzP3JKYw3ynnWFKK/v+HP4JtzYIkQl6kxB+86pK0VtEeenEd1NrGkGWiY08Y8dqgHtHcgx2Kf1BBG4yTJM6ykrRRZr/u/IpbG7dAA6dSNY1/6KgLBptc4HieI8uU5sbt2zoNPFJWjLYdrSNYMcaD+S4pFgLG7QOIlvcxKiXtIjcTJ1nTT+VzuTGxhgkmNBpzokrRO2t+3cJLI1eeNEMEhstkGSJ18P9qVdogje5oLddJnwb/6kSJEAN5Pcaaxzu/O3JJYyB5948/wUREwe8x2hOSCAB8SY1SDg7kcd/h4pIf/0uM69sbkUFsAbXuMubE7tfaPb+b/AJ6yXe0ggRHBP+5aXW7bRbRta0AMdERxu+jZ/wAJv+ksp9tpO5zRr30HwTY7MlsQT8+CSpB+2Y0B799FDc+OD/uSLncbRr4TKKLUfoxw1LXx5GsKJJ10jxHZLc+IA0P5Qki2YcWngOn4pgAR5ePdQL3J5cRxwkq12gGYHwTiCCBwDMqLQ4kjbKW5zdSI8j5eSSmREHuU4j26aHnxUdxJ4A8DKQcSIjnzSUycIg7eRPkUuDECAPxUS4wNAC0H56+SRJIgDgeKSbSObG1pbB00I7Hum1kiAToAovc53u2iNB+CbcQZgaR+B80lWldLXGW6jRwIjXw3KO0tfAJMHkAD8qY7vpQNTp81PcZkDQRqOf8Azp7UlMQwl20uLdSD8Pg1IMJdGp+7t3hT3e52gYBMhp/8kXJNc7dO0c6gHTzbt3bnMSTQf//T4j6wMcLMfdDXNY9pg92uFbpjasja2eZ85/uK2eu1lj62w4Fnq7WmTIDm7tsgfnB9j3LHO9rROnl4psdgvWhmkgz8+EoZpumO/P8Aekdfh4pw8D+On4IqRkNjgptvxgqbd0SIMdim1IkyPxSQtA+SQDBz/qU8x4z2SjXkj4+SSmTWtDSSOdIgqECNRH3p4+AHxTHx/wBdElL7W+ED5pw3tGsdym450P4ypQ4aAETpASSw2zI7qW2QfLkd+Ew1Md54UjuAgAkcxH5UlMQJDYHCct9sxxM+Mp2tfAcQC06iI/1b/aTO3BvcgePASUoCTx4JwwEg7j5f6/1kx3N0dDpEgiO/wTscwbpg+0x/rLUlLRHLjp2TitpJ11gaJg4DcJ+PPPina9uxw0kxGnH4pK0f/9Ti/rE1/qUkkxNse0D85jd39v6W5YpEAERB0IjUHzW39YpFrHHVzX3l0/1qx+8Vjte1u5rhukQCNIPyTY7L0ceXfuD/AHpbRE7eYLeUjtJM+3Qx5x+amkng8CJnt4aoqXDBtJLTHj2lNAgac9/BTJcWwTo46gRE/wDRTEhzjuAbPMf7ElMdOTr8E3Bn+KcxPaOyQIGv4JIUfAcdvApw0QD7fMF0H8qYEd9QZ4/1Kk1zSf3TPy+EQ5JS81wQGw7tB00+PuQz4+J79/xTlvu0PwPf+CRaYmdDxKSlwYZ/KBMEaacKJH+7/Up9up1+JGqRYRGvnqeySlfHU6CE0amIkfBOAO5geASLYMEgx5pKXa0O8CfFMPpDiPh+VOxxaHDeRIPA1J42yk8iQSIjsklYaknxPmk0wZPh4KQ7GJ1nwKiXDdPc+CSn/9XjvrSa/tLNJa23JaBvE6WNDT/237f5awTWOwMeZC3PrKQMlgaRta++HD3GA9v9X+y79xYziJkEDw5n4D+qmx2X0jLPjPxEJbY7Ge0oneCAZPB791F23y18P4FFVMNmukxMDUJ9ABtmTynO0gwTPaOEwJH0TqdNOUkMQ3UQkWEnv4a9lIzAcfhMBMYBO3jzRVS2zy1+IhOBr3B7KQEjxjWE8fyoBOpEcjw/OQVTCAWjkuOk6QnDdZgnkjzTmCSS7U8lIx8o7jVJSxaPPz1SDYnQyI5jxT+34eQSJE68TpKSlizyPlr3KQGnHx18vgnEnQDzgd00iJ7+EeKSlbBP5Ne3kk10OBAE/D8u5OHmOAfPvxCgD/r3SUzLiew+AHz8UmvIJLdCRHAiP6qYl3hz37pgTpH4c6JKf//W5rr+NhX075fh277ThtuEMsBLbLMdxLWPqe6a76X/AM1X6vo3bPV3rlodt3bTA7jRdZ9ZMfDtNovy2499eTkGktr3ssrca9zHel76vQ2t9H2fpmWfov0K57Kw66NcfOqy9JIqbc0gzt2/pqK/zf0n0/8AwT2JkNtV7U9xP0SkQdpJadogEkaT9JL9JpxH5v8ABGo+07XBm2J95dx/Jnf7P6qcpr6azp8U7muY7a4e7uNCnsA1MiT9JsGB8FK4viHBgeHHeR9Iuj87VJCMzEkHUqQdOgGnBMT/ABTe/YCT34MqVYEaugfnSB/0d/5ySVi4kyAdR8Pmm1iSDEHVEf6gd7TIM7Z7ae76PsUB6nlE6pKWeCCQ4FrhoR3EJw0kEjhuruNPvR7vWLf0+1rSdP3hp7e/qfR/eQWS0u+i6sAb5mCJ9vdrklMAZjSNU5B27+W8TpzCkdd2+G+4wWidfkfop6y8DUMIJO3dwHR7okt/8gkpgATwD4+STXeUk/69ikwOnQ6R5/ik0GdT906/ekhlu7mQeQYjU6KBmOQe51HgjEDbyJHOjIJj+SUMfh5pJLAOeJ1AnnXVOZ8ZjTnz7J/HSPy/JPrJn58Qkh//2ThCSU0EIQAAAAAAVQAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABMAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIABDAFMANgAAAAEAOEJJTQQGAAAAAAAHAAQAAAABAQD/4Q4paHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAxNi0xMC0yNFQyMDozMDozMi0wNDowMCIgeG1wOk1vZGlmeURhdGU9IjIwMTYtMTAtMjRUMjE6MjQ6NDQtMDQ6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMTYtMTAtMjRUMjE6MjQ6NDQtMDQ6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvanBlZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9InNSR0IgSUVDNjE5NjYtMi4xIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkZFREE1OENGNTE5QUU2MTE5RTE0QzQwNDg5QUVBNzNEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkZEREE1OENGNTE5QUU2MTE5RTE0QzQwNDg5QUVBNzNEIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6RkREQTU4Q0Y1MTlBRTYxMTlFMTRDNDA0ODlBRUE3M0QiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOkZEREE1OENGNTE5QUU2MTE5RTE0QzQwNDg5QUVBNzNEIiBzdEV2dDp3aGVuPSIyMDE2LTEwLTI0VDIwOjMwOjMyLTA0OjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY29udmVydGVkIiBzdEV2dDpwYXJhbWV0ZXJzPSJmcm9tIGltYWdlL3BuZyB0byBpbWFnZS9qcGVnIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpGRURBNThDRjUxOUFFNjExOUUxNEM0MDQ4OUFFQTczRCIgc3RFdnQ6d2hlbj0iMjAxNi0xMC0yNFQyMToyNDo0NC0wNDowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDw/eHBhY2tldCBlbmQ9InciPz7/4gxYSUNDX1BST0ZJTEUAAQEAAAxITGlubwIQAABtbnRyUkdCIFhZWiAHzgACAAkABgAxAABhY3NwTVNGVAAAAABJRUMgc1JHQgAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLUhQICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABFjcHJ0AAABUAAAADNkZXNjAAABhAAAAGx3dHB0AAAB8AAAABRia3B0AAACBAAAABRyWFlaAAACGAAAABRnWFlaAAACLAAAABRiWFlaAAACQAAAABRkbW5kAAACVAAAAHBkbWRkAAACxAAAAIh2dWVkAAADTAAAAIZ2aWV3AAAD1AAAACRsdW1pAAAD+AAAABRtZWFzAAAEDAAAACR0ZWNoAAAEMAAAAAxyVFJDAAAEPAAACAxnVFJDAAAEPAAACAxiVFJDAAAEPAAACAx0ZXh0AAAAAENvcHlyaWdodCAoYykgMTk5OCBIZXdsZXR0LVBhY2thcmQgQ29tcGFueQAAZGVzYwAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAPNRAAEAAAABFsxYWVogAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z2Rlc2MAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHZpZXcAAAAAABOk/gAUXy4AEM8UAAPtzAAEEwsAA1yeAAAAAVhZWiAAAAAAAEwJVgBQAAAAVx/nbWVhcwAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAo8AAAACc2lnIAAAAABDUlQgY3VydgAAAAAAAAQAAAAABQAKAA8AFAAZAB4AIwAoAC0AMgA3ADsAQABFAEoATwBUAFkAXgBjAGgAbQByAHcAfACBAIYAiwCQAJUAmgCfAKQAqQCuALIAtwC8AMEAxgDLANAA1QDbAOAA5QDrAPAA9gD7AQEBBwENARMBGQEfASUBKwEyATgBPgFFAUwBUgFZAWABZwFuAXUBfAGDAYsBkgGaAaEBqQGxAbkBwQHJAdEB2QHhAekB8gH6AgMCDAIUAh0CJgIvAjgCQQJLAlQCXQJnAnECegKEAo4CmAKiAqwCtgLBAssC1QLgAusC9QMAAwsDFgMhAy0DOANDA08DWgNmA3IDfgOKA5YDogOuA7oDxwPTA+AD7AP5BAYEEwQgBC0EOwRIBFUEYwRxBH4EjASaBKgEtgTEBNME4QTwBP4FDQUcBSsFOgVJBVgFZwV3BYYFlgWmBbUFxQXVBeUF9gYGBhYGJwY3BkgGWQZqBnsGjAadBq8GwAbRBuMG9QcHBxkHKwc9B08HYQd0B4YHmQesB78H0gflB/gICwgfCDIIRghaCG4IggiWCKoIvgjSCOcI+wkQCSUJOglPCWQJeQmPCaQJugnPCeUJ+woRCicKPQpUCmoKgQqYCq4KxQrcCvMLCwsiCzkLUQtpC4ALmAuwC8gL4Qv5DBIMKgxDDFwMdQyODKcMwAzZDPMNDQ0mDUANWg10DY4NqQ3DDd4N+A4TDi4OSQ5kDn8Omw62DtIO7g8JDyUPQQ9eD3oPlg+zD88P7BAJECYQQxBhEH4QmxC5ENcQ9RETETERTxFtEYwRqhHJEegSBxImEkUSZBKEEqMSwxLjEwMTIxNDE2MTgxOkE8UT5RQGFCcUSRRqFIsUrRTOFPAVEhU0FVYVeBWbFb0V4BYDFiYWSRZsFo8WshbWFvoXHRdBF2UXiReuF9IX9xgbGEAYZRiKGK8Y1Rj6GSAZRRlrGZEZtxndGgQaKhpRGncanhrFGuwbFBs7G2MbihuyG9ocAhwqHFIcexyjHMwc9R0eHUcdcB2ZHcMd7B4WHkAeah6UHr4e6R8THz4faR+UH78f6iAVIEEgbCCYIMQg8CEcIUghdSGhIc4h+yInIlUigiKvIt0jCiM4I2YjlCPCI/AkHyRNJHwkqyTaJQklOCVoJZclxyX3JicmVyaHJrcm6CcYJ0kneierJ9woDSg/KHEooijUKQYpOClrKZ0p0CoCKjUqaCqbKs8rAis2K2krnSvRLAUsOSxuLKIs1y0MLUEtdi2rLeEuFi5MLoIuty7uLyQvWi+RL8cv/jA1MGwwpDDbMRIxSjGCMbox8jIqMmMymzLUMw0zRjN/M7gz8TQrNGU0njTYNRM1TTWHNcI1/TY3NnI2rjbpNyQ3YDecN9c4FDhQOIw4yDkFOUI5fzm8Ofk6Njp0OrI67zstO2s7qjvoPCc8ZTykPOM9Ij1hPaE94D4gPmA+oD7gPyE/YT+iP+JAI0BkQKZA50EpQWpBrEHuQjBCckK1QvdDOkN9Q8BEA0RHRIpEzkUSRVVFmkXeRiJGZ0arRvBHNUd7R8BIBUhLSJFI10kdSWNJqUnwSjdKfUrESwxLU0uaS+JMKkxyTLpNAk1KTZNN3E4lTm5Ot08AT0lPk0/dUCdQcVC7UQZRUFGbUeZSMVJ8UsdTE1NfU6pT9lRCVI9U21UoVXVVwlYPVlxWqVb3V0RXklfgWC9YfVjLWRpZaVm4WgdaVlqmWvVbRVuVW+VcNVyGXNZdJ114XcleGl5sXr1fD19hX7NgBWBXYKpg/GFPYaJh9WJJYpxi8GNDY5dj62RAZJRk6WU9ZZJl52Y9ZpJm6Gc9Z5Nn6Wg/aJZo7GlDaZpp8WpIap9q92tPa6dr/2xXbK9tCG1gbbluEm5rbsRvHm94b9FwK3CGcOBxOnGVcfByS3KmcwFzXXO4dBR0cHTMdSh1hXXhdj52m3b4d1Z3s3gReG54zHkqeYl553pGeqV7BHtje8J8IXyBfOF9QX2hfgF+Yn7CfyN/hH/lgEeAqIEKgWuBzYIwgpKC9INXg7qEHYSAhOOFR4Wrhg6GcobXhzuHn4gEiGmIzokziZmJ/opkisqLMIuWi/yMY4zKjTGNmI3/jmaOzo82j56QBpBukNaRP5GokhGSepLjk02TtpQglIqU9JVflcmWNJaflwqXdZfgmEyYuJkkmZCZ/JpomtWbQpuvnByciZz3nWSd0p5Anq6fHZ+Ln/qgaaDYoUehtqImopajBqN2o+akVqTHpTilqaYapoum/adup+CoUqjEqTepqaocqo+rAqt1q+msXKzQrUStuK4trqGvFq+LsACwdbDqsWCx1rJLssKzOLOutCW0nLUTtYq2AbZ5tvC3aLfguFm40blKucK6O7q1uy67p7whvJu9Fb2Pvgq+hL7/v3q/9cBwwOzBZ8Hjwl/C28NYw9TEUcTOxUvFyMZGxsPHQce/yD3IvMk6ybnKOMq3yzbLtsw1zLXNNc21zjbOts83z7jQOdC60TzRvtI/0sHTRNPG1EnUy9VO1dHWVdbY11zX4Nhk2OjZbNnx2nba+9uA3AXcit0Q3ZbeHN6i3ynfr+A24L3hROHM4lPi2+Nj4+vkc+T85YTmDeaW5x/nqegy6LzpRunQ6lvq5etw6/vshu0R7ZzuKO6070DvzPBY8OXxcvH/8ozzGfOn9DT0wvVQ9d72bfb794r4Gfio+Tj5x/pX+uf7d/wH/Jj9Kf26/kv+3P9t////7gAOQWRvYmUAZAAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMAQcHBw0MDRgQEBgUDg4OFBQODg4OFBEMDAwMDBERDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAFdAboDAREAAhEBAxEB/90ABAA4/8QBogAAAAcBAQEBAQAAAAAAAAAABAUDAgYBAAcICQoLAQACAgMBAQEBAQAAAAAAAAABAAIDBAUGBwgJCgsQAAIBAwMCBAIGBwMEAgYCcwECAxEEAAUhEjFBUQYTYSJxgRQykaEHFbFCI8FS0eEzFmLwJHKC8SVDNFOSorJjc8I1RCeTo7M2F1RkdMPS4ggmgwkKGBmElEVGpLRW01UoGvLj88TU5PRldYWVpbXF1eX1ZnaGlqa2xtbm9jdHV2d3h5ent8fX5/c4SFhoeIiYqLjI2Oj4KTlJWWl5iZmpucnZ6fkqOkpaanqKmqq6ytrq+hEAAgIBAgMFBQQFBgQIAwNtAQACEQMEIRIxQQVRE2EiBnGBkTKhsfAUwdHhI0IVUmJy8TMkNEOCFpJTJaJjssIHc9I14kSDF1STCAkKGBkmNkUaJ2R0VTfyo7PDKCnT4/OElKS0xNTk9GV1hZWltcXV5fVGVmZ2hpamtsbW5vZHV2d3h5ent8fX5/c4SFhoeIiYqLjI2Oj4OUlZaXmJmam5ydnp+So6SlpqeoqaqrrK2ur6/9oADAMBAAIRAxEAPwAl8w3Hlhifq1zLptha0jeCEq6zSFiArJXnxam7KGzXByGE/mJFFqmox69pFnItuIVguYLcVMM0e1SDvxPyy3FLoUg0w+90q9t9Gi1aeaRJLmQpbW7KebhftvSlFVcujIE0pkeiVSzXXFQUYHqjMR096dcnQXiKw3N3+woUHrxpvjSDI9y6N7wElVR1/aVgBTHZRxNSz3ZUrIodQagqAaHwB8MFBPEervsTBmj+N1rUbgDCp5r3uKCjIRQf3qEEj7saSZBHQawF076vLBNOdwj7BT4dMHDukZDXIqVpFLcItutu7ITT1HPMoTvQDxOFiBfRqaGeMM5iUtCeFAQQPdqdDiptBKeLczIEJ603B9t8LC1kzyvcB5CByHEAClB2xQeaIDzQhH6pHsh7g4Ge49ys8k3B5ImPxfaiNGX7sUknmFT99aW/qJAnCXceoRQHucCdwEMymRkqgkZhUKGoPowsVnppEGoqyFu244/7LvjuiqUrZZZJWRKUArsf498JRGyqsZFIdYkoh+JiDQ+2BJaaZVlU8lFDU0HSvah64aW6RTGMFSrA1B5cgVH0V65FkglMjPwDAb14mvHJMdyrqpYn9t0+1RvhHtTAUjdCyoyz8eNS29B0wsCN2zJxFVqZPnt8saTa0QvMhqpBXcjFAFtD+7owUU/ZPU/LFLlUyEAkb9Pb236Yo5tcXUlUPIDuNxiilzKyEUUFv2qiopilytLFRlHwnsfH2xWyF86s6lvsbdB3+eISQsC+oQtfkT2oO+KFvF1JVDyA7jcYopcyshFFBb9qoqKYpcrSxUZR8J7Hx9sVshfOrOpb7G3Qd/niEkOWRmVY1oQepddq4ptszcWKcV5D7SkcTii91qSukJUKhJNeQ+0P8mvhjSg0tELzIaqQV3IxQBbQ/u6MFFP2T1PyxS5VMhAJG/T29t+mKObXF1JVDyA7jcYopcyMGACgsdmJ6UOKaWD96SwBLDepGKLtdTkrMqnn+106YqrKSfTVUFCepOKQ3cpP6bIaiNT8DUFSPDbEFJBWLIzKsa0IPUuu1cU22ZuLFOK8h9pSOJxRe61JXSEqFQkmvIfaH+TXwxpQaWiF5kNVIK7kYoAtT4v/AL77UxWj3P8A/9Ahh8uXlreyeabNPRmtmMIluokJQ92Q1Nf8k5rxsK6N53KTSaha6HcNqWsXsInaKQwSQIoe4eQ1JkCnnLL+yrtkhG+SmTzXzD5nvNX1H1/jEaDjb2r7qoPUn/KbMmMAAvF3JOkpDMXXiGO/Enb6MmgHd3qoKgMxTxUUH440nicJncUrItdjvs3zONIBK2HkoZViBRuoqaYpHksKzHpTwApt8sWO6sTIFoNpG2psN8AZ2XR3M0JCtz9QfYIPwgHqOOGmIkQinublmPoFoE2LsDWp/wBUYKZklRKToWHEEMKl+zfPFiRSnJLOCPssH+1GAKAfPCiyox1eU8vhYmgr2GKAi3gNEVmLgHcA7N7muLKlksaxStW5YyLTZSQQDipFHmrtRAa3RldR8O9QK+3jgZcuqgJCa8yWdTXmtFPyxYgqUl0SnAKyjo6k9T92GkGS2EVqFHAjZjSuxxQAqCRqgJRTGOLydSfkMaTageAk4n9o9SMWKKcrurupRN6Gp+gb4GZPeogfCWAZK/Og+WFBWBuR41+Knwjff54oVBTga15nb5DFKkjRsRGqk7/a8MUAhWaKQE8iUB2JrUfPG2RBWhTKOPI8l2HTceOKGuSrzU18A3Yn6cUWpqqcKqjcz1PY4qF9HWgNFodhipC9olMgJQkkV3O5xTw2vEhhIDkKD+0RXbwxTdc1Nnhq/AMEP2Sdqn6cUEhSVU4VVG5nqexxQF9HWgNFodhipC9olMgJQkkV3O5xTw2vEhhIDkKD+0RXbwxTdc2w0DchEOKkbl+/yHbFTSiwrULEATQK1fbFj8Hem4orbgnoOxxTS9opATyJQHYmtR88bSQVoUyjjyPJdh03Hjihrkq81NfAN2J+nFFqaqnCqo3M9T2OKheVcDgfh9h13+WKkNRiQD4WotfjHtiVCILQHj8J4kb9tsU2pOEpSNxUfhihWV/SjR3+Av8Atn4lI9hiyBobtBoG5CIcVI3L9/kO2KmlFhWoWIAmgVq+2LH4O9NxRW3BPQdjiml7RSAnkSgOxNaj542kgrOT+J/l+j+bGmL/AP/R5pPcwyyeRI/UkWFYSLktJIUmcPUg7kbe+Yl/U3AcmHee5Jm87avcQw8IxMPTQLsF4inH2y3F9IU2CkBluS/N42avTsPwyzZFlzTO5+KEcvnv88VMvJ3JyoURbDrUkVxpbbBu2WnpjivfsPnjstlbLJODw9MKKVoh6++ICmRWJPdKar8PZjTHZFld6t2KFuJ8Cw/HHZbK31bkHkQCRvU71x2UEqgu73lzHENTr4VxoJEi213eiPcKAOpp9r540F4ipL6jVJ48m+7FC1JbpHHwip2HIYVBK7ncNKC28nY7gfRjstlWLXrqRxBA6mlSfY0wbJstBrpEWkajjvQrv9Jx2XdYZLmUfEEFPAb47IslTd7ioqwJGxO344dlsqiK6owV/hb7W43wbK707hUBUqtdxQiuK7rBDcMTWhB3JNN8bXhK/wBC5ElaivXbidvlioBWO8y0+LkRsuK7tK1xXtyB6EY7LZVHnuiQWCkjbpQ40GXFJoyXXFiE4lvtUG+FFlYJ7gRcGWqnpXBsjiLYmuVdXVByj6GlcK2W3e4esjJyLdadPuwbKSVqm5NBQgjoR1xUErhJPTjwrTYeOK2Vkktw5UMv2fsqPbHZBJKtLPcSKFMY4V6eJxZcRK2Qzbt6dQdqDpipJWKbk0FCCOhHXFAJXCSenHhWmw8cVsrJJbhyoZfs/ZUe2OyCSVaWe4kUKYxwr08Tiy4iXGa4DArGqkftfLFbKm5uJHJeisxqR03/AIYo3vdsG5jDKp7bgb/TjsmytE9wIuDLVT0rjsjiLYmuVdXVByj6GlcK2W3e4esjJyLdadPuwbKSVqm5NBQgjoR1xUErhJPTjwrTYeOK2XOCYlUA06+4HvikldDIY2HCnDvXeuKiVLpZFfcjk7beG2BJNul5ekiKA6rsan+GFSTyaM1wGBWNVI/a+WKLKm5uJHJeisxqR03/AIYo3vdsG5jDKp7bgb/TjsmytE9wIuDLVT0rjsjiLvVuP5R0phWy/wD/0ueajaxR2/5fThCsDRySXFD/AJdNlzDP8TeDyDCvO17bf4y1V4y4jSUCICpFQvWmW4h6Aky3Sg3tuykFJWc7sDsDXLaXiCi1zGp4Lbld6setBixsdy8Mv7CSsrdQf4Yp9yyWU0bh6itsG5DagwKSoNcUpXlU9KUpT2yTG25Jo/hAQhh32wJJW842BBFAe5ocWK0yNWm7r0AqKYraotwa8mVQV2Va4ptTlkLcgwReXcdcIQVhdfSKqB7mu+LFbGw3WlR7mh+jFIRVvcIJOXEc6UoTXBTKJVkv1QsUCK5NepFMaTxqElyJG5M1eZ+zUin9mNMSbU3MQqzMGFaKAT9+K2FJilCahq/fhRbSOQhoakdj2xUFcrwjps3UNiuyKjuolccmBBHxELWuABnxBYZLVW+ABmrUVFB8jjSLCzlHQleIbfft9Awo2aiuOJryoQaqTucCiS5bha1YqeR+M9caTxNtdowAVQnHalT8XzxpHEsBjapZh+r6MK7NCVEXgnHfoTvTFbpsyw7CikdTSu5xRbZkhXZTTvyBqflimw2Zl/a40bcgCg+/BSTJsSQPVTsg35V/Xiiwt+sAhd1A+yF32HjjSRJoyw7Cg4jr1qcLG2zJCuymnfkDU/LFNhszL+1xo25AFB9+CkmTYkgeqnZBvyr+vFFhb9YBC7qB9kLvsPHGkiTZmg4gBVDKdmNd/fGl4mpLiInYDf7VB+o40pkC76wQBUn2U7CngcaUyLQMbVLMP1fRhRs0JUReCcd+hO9MVumzLDsKKR1NK7nFFtmSFdlNO/IGp+WKbC9ZvjX7NWI+ECm/bfBTLiVZIEJ5AlaHqcAKkKZgJlJBA8Vw2ilRLVSGBZadKk9MbSAFOaIQP1Wn2anfrjzXksM0HEAKoZTsxrv740jiakuIidgN/tUH6jjSmQLvrBAFSfZTsKeBxpTItAxtUsw/V9GFGyzjb+334ooP/9OA69as6flzZiWlvcwvyZaV5rJ79B40zGI+rvbY3fkwnzmRbebtajBg5Ry+nydyanj9pQBk8fIM7ruSyLTrlo+aUcgBuAYitfcjJWy4TSGeO9o7qiiMbA8hsfnTfCCGNFSUzLxEk7AnrSgp70xKi+pWu24ElyW33FOvy2xRXm5USgb1GdR+zQAgfdioj1W+nbEmgcdy3bFQQujto2DbsVXqKiv6sVAajjgLqfTYEg0qAenSnzwrQdOLReFY2BNeWw64i1NKFEIPwnkNxXYEYotcwgMQpCRL3b9kY7qark1GEU0pzp1IAIOKG1EbSAMvpDorca74qOa8pGoAWpcfaqtKfLFLgyCMsUq/iV2pit7KbImxFTy3I47ffhRQUCFJYEVPRadMWKoiAkBuCgftYEuVYjVeYHgab4rs28MQVf3nLxUD7PzwpKmAOGzL/qkYoaQIdq/F8sUN8Yq/DUnvtiq6g4/EOVOtRSmBLljVgWSpA9hTFea4RIsQLH467gAEU+/CrTxKCDHt416YqQ2ELBSOgPxCgGBaXRxxlGdiNtwf64VCsqW+wkJHMVQgChGBlt1ahEPA8ga1+HiAy0+VcVFNiKFuRiJb+YFQDX2wL7llzAwmoKALQkClOgPXCCsorI44yjOxG24P9cLEKypb7CQkcxVCAKEYGW3VqEQ8DyBrX4eIDLT5VxUU2IoW5GIlv5gVANfbAvuVZ7SRQCDGvIVVaA42yMShkC1Yy0BHYDYbbUyTX1dInKFWFSDvv1226HAk8lMRIsQLH467gAEU+/Chp4lBBj28a9MVIbCFgpHQH4hQDAtLo44yjOxG24P9cKhWVYBT1K1YckIAoQMDMKQlNQ0j1cd/7MaYWq84jUt8VftOOtfbFk0wQRD0mDEH7DYr02ckXLk3IOT1UDfFQFae0kUAgxryFVWgOC2RiUMgWrGWgI7AbDbamSa+rpE5QqwqQd9+u23Q4EnkpiJFiBY/HXcAAin34UL/AEbfwP8AbgXZ/9TnmrTGaL8v4ooiL4RP9V3+Fg0nxO2Yl7ybwBQYP5wWOPzfrKyJ6TifiyEjY03NffLsd8IUUk9Yo2Rkmb0+jIWNDkk2FQ3Nuz8QiLGCCJN6/QuNJBFrGFv6zureonUMBija1J7gA9qDqKfxw0xtcs4rwUU5bk8f4YEgtiVSDwFVr33ocQF4mkkkYGJEIUH4iK74qLblkZVAAK02Y17+2ISSoKHLfuwyk9WOFiFsjyEHkab0G1MQglR4OIqnlxJ+EDxwsaXojBCpBJ8R4YpC+F0qF7D9lulcCQqSGQcWAUE7qQan5HFTzUvrN0WIrudiRjSLKmxam4IBNCa74q03FT8IJ8fDFSuNClUAJH4YVdRzuVG/Ud8CqywtwahpQdT3GKVN0YCg6nqo64rTSxjj0AY9u+JKALaKEFdq8eg74VpzcqcmIqfvr4YqqwrVGBSm9ak7VwFIXu0fBwlGfvUd8Qk0hUUI/JwTx3IXCwpEpACPWUUB34nwwMxHq00RTkOIp1FOgr4YoqlWOT1k2RSUFNxuPkMFMgbWIhDUKqit9o13+XtiildoI4h6RCiQ7rQ16+GNsiFAW9CZNvAjttscNsRFa0RTkOIp1FOgr4YoqlWOT1k2RSUFNxuPkMFMgbWIhDUKqit9o13+XtiildoI4h6RCiQ7rQ16+GNsiEM7BCF3HbiDQVwseTaLIVEla1B5V3pTahxQB1csjOnpuNk3Svz8caSD0bdo+DhKM/eo74hTSFRQj8nBPHchcLCkSkAI9ZRQHfifDAzEerTRFOQ4inUU6CvhiiqVEkMyHiickFN9iMFMgbQ6IvqDmqg+52wsKbDRBPhUF6/F4Ae2FOznkr8Sjl28MClUiQh+LADkvwjvX54pAadghC7jtxBoK4rybRZCokrWoPKu9KbUOKAOrlkZ09Nxsm6V+fjjSQejbtHwcJRn71HfEKaQnoyeI+/vhYU//9Xlt1JLLaeTbYAGWcuVuq/FwR9ox/Jv4ZhA7zLkDkAxXzlIT5u1YyxokolHMBiQDx7V65fi+kKSLSBWo5cSEt+yoPX3OWMFVGuDV4pVUAV4tQ0+8YpF81oF0VHFhSnXuMC7qZ5VXcOehDb/AIYVLf71GO/Fl35r4eGK2u+tyiNiQlG6EClcaXiLafWaBg1BTfsPlikEqnKSRasVjCmnwipr74E2pEPITHyKeFcLFzwhaopLMx3NanG1pbIpUMof4U2+WKKWIGK7OCe3v7HFXKpVCOgrv4A4UOAO7Kte2/SvjirZglChv2j2wWtNlOLEMh5EfGD/AAxSpniPhClanbkdsWKImjKqvCMty7j2xZyCiFNKkEGu+++LFfCXSTkASp+0tcUjm3Kzk8nAXwYGtcUlSDLxNP8AYnFjbipKVQGvVcVpTPJX+JQO9MVVIZeIPIclc0FN6fRhUFv1JU5KCONNqjemBNtRCiFm3IG3gfnigOSQ05SbKT0HXFQiHVSAI5GKjdgdhgZkdy2NwsEhagPIcJPD5YUDk5pGZSx3JFOY+1it2ryMqRLQFWp8ZpUH2rgDI7IQOFJL1EZIoP2sLDZWdVIAjkYqN2B2GBkR3LY3CwSFqA8hwk8PlhQOTmkZlLHckU5j7WK3avIypEtAVanxmlQfauAMjshiQUApU9qnemFid19IWQBahupavh2pgTsVNXC8ju6MQu3Y74WN0t9SVOSgjjTao3pittRCiFm3IG3gfnigOSQ05SbKT0HXFQiHVSAI5GKjdgdhgZkdy1CDBIG4ksaI/wDAYUDkpIDIw5SkU24Nij4tzSuklCxamyjqBiFJVEMse4uAWbqG2/rikA96/wBSQI37wEdeNe3f5YpFrKIYFcsp5VoA1WFOx8MC8wpySIEULGa1qWDbfqwsSW1YrWtDzHQmlMUhb6sighCCp23G+KLUayeJ/twsaf/WgxFnHY+SZ+C+spYeopqEVpPi5L7+OYPWTkg7Bg/n5YovO2uL6ZWH6wOEmxABUUBy/F9ISfcxwtxlbggcAbEDt2y1ipMzmlUT4jUqux/HAhcZZVDFYuEi7MTsBXthW1JHbmoWL4uh3rvixvyVFkVWYtUliQafaB98UrUAqSfoG3XFVVzCyhWBNN27L9+BOyxZJUUiNVVH+H4jucKLPRSkDuVVqGg6A7ADFBWKxAPFiq12oP44oC/kxQ0p4GnVifHFKlQmhGwXY+2KKXkqdxvT7XtiluMRs4Ar79cSoXMFZwQWSReo7YqVrvIGbhSjdieR+jFFtLEwYMtH7774qvYTuh6EV3pX4sU70sMb8dzuOv8ATG0U74wKj4ewp2wq4xzAVBqG7dsVIdCjs5BPE028MCgK7c+IQsDy3oB0pgZKLRFXIYglu3thQQtdU9NVVgsldgMKFQSkyqCPYcsCSd6XyQOqmi9dyD1JxtSKUXUrxanX9k/s4UEL1SRjxO3Lv7YE0798gAIHEnaor+GK7rijypwQ8QDVi3Q/I4rSu3rekvIqafaHUEYGe9IWcqWEiCqtQUHQbU2wsC2qSMeJ25d/bFad++QAEDiTtUV/DFd1xR5U4IeIBqxbofkcVpXb1vSXkVNPtDqCMDPelN54GWigVI7djhY8QU/TLEFTyQ0HIePfFFLZ4JUUVJCk0TELIFsSkyqCPYcsVJ3pfJA6qaL13IPUnG1IpRdSvFqdf2T+zhQQvVJGPE7cu/tgTTgJV4oQApNBt9FcVorWiYv/AHZBXqa742ilb00kT4XCk7pStScDPZ3pMnxS7tSnBtjv4bYUclF0kAYyBWWtBU7/AIUxtFHq0VVwPhBHRuO3ypitLwYljZWVj2B9sU7LJAFARfs+NPi+g4oK9aopcrWmxBG3sdsUqdbn/fQ/m6Yoo9z/AP/X575ijuEh8hWKBIfXZ29U9JQJqKCRmGI1KbeDUQxHzzJLF501yNgAVnAZDQlaLuPfLcQ9IZ8TH6BYmMZ3BrU7CpyxHRQY/Cxajct+fQ/QMUNtMjRABgCNqAfjhpBksPp8gpJG21BU4FtYqLx5k08B3+nCxbKBNlfi3cUr1xVEFqr6a0cDf2B8cDK+jUbL6gLKrb7FhT6BhQCulRT8TUVCfsnAyUX4wLyAqTsKdsLA7KLRpRW5ci29R1GKF/piOLnE5qftKe/0YpppVcruat2ptt74rap9YCREJs9aAMPxrivFsokv6vIbGlORNcKG5FiBZulKfH74Elakw47glv5ux+eKLXpcuo48RUdD2xpILgWdipqp6jeopiq5UZqqrV47ewxV3HhGVWT4gftdsVU0QcjQ81Tct/DFVRZkCPQlVb7IpUg++JW1I7tzr26nFC0UJIUkD+bCraVUUoSetfCmKq/1sOKxglwKFidsFMuJfyJoQ3Gn2i3icCWw06FmK7L0pQ/ThQsNzJLxA+MDq3HrjS2rwsOModKMP91AdSe61wMgWgWj5LK3pQkbRMKsT4BsV5e5Tqpp6Z4IuxrvvjTFsNOhZiuy9KUP04VWG5kl4gfGB1bj1xpbV4WHGUOlGH+6gOpPda4GQLQLR8llb0oSNomFWJ8A2K8vch5WQkBBwQbHv9+FgVSJJdihHECoQbU+eKQFKSaSSPiSwHLcnx8MVJUkqopQk9a+FMLFX+thxWMEuBQsTtgplxL+RNCG40+0W8TgS2GnQsxXZelKH6cKFOS5kkQAfEADVuONLZWs8HrhQeSnYmvQ47rYtdKeEgMEgVjsw22964FJ7m1KglZJVo37Z+I4U33octbhjtzFTXfr74sLCoJbf+8J3Xqg6GuNMrHNeZIAgaIgE9R4DFJIrZqBolf03kAj6q5PQ/LEoBHK1JqAMA9VrU16H3GKFtIv9+t0xV//0ID5g1Fxd+QY/qyMYDKYDK1FJ9c7in6jmJ1n724bAME8+3Ik8664ZYyZZLrkSetadNstxfQFPPkkgkZK87cln261r7ZNN10UpCSdod/2SMLG1MvKCvKKhB+n+zFVkkjiQ1Qq3sf44oNrkkkVQwBBH0jFbK5J1NefwkjrTtim21uCo+F1FNgTXpjS2qxyRuRyKsg+0K7E+2Kbd6qtMJJHAjWqqnWmKOawyxFQoYBV3Pc1xWw5UtwwYShid/Y+1MU7KVYSzUkC8tx7YsdmwxCHj0Pws7dPmMaVSqOQqxZV9umKF8bQEnkpFT8JxSKaKVNNwlahT398KttyqFU8h3I6YqV6lv7rh8XbxpgVzCJCevWvE/1whPJuaROkZorEEN9lvpwKS1KVI4gmhHxU7/PFVJWhQNuSSNh2+nCxXxPEu7GtB9FcBSCsLc2BLAAn4RhQrMqcSUcMF+144GRWMxC0DAA771rii1MEkAK4BPbFCsiybguCiCpJrQVxZBsO6JVXPE9a9KYpaSdkIIYBelBuMUAq/rTMNpE26PQ1r4DAyNqMrXMo4vIAq7gHcfRhYmy0odjXkvBRU+A2xUNh3RKq54nrXpTFLSTshBDAL0oNxigFX9aZhtIm3R6GtfAYGRtRla5lHF5AFXcA7j6MLE2V6h+AKuladAK/filSUkKWrR/EHFFNBuXJgagddsUOZiFoGAB33rXFbUwSQArgE9sUKyLJuC4KIKkmtBXFkGw7olVc8T1r0pilqOd0YFTUVoFHfFFqf1YChC1r1FRt88bRwrjDApoKEjfrsfljaRELjbKGXkqgN/lDG08LXpKhI4g02496YgopoxQMDQ+4alB+OIUgKRiWlVUke564WNLvQi9PkfhJNKE9DinhC54FNAE+ICpAYYEU19XH8h/4IY2vC//R53qjXK3PklrNoVmVZOKz77+qahQf5sw6Fzvvb4n0hhvnbUjJ5s1cHhDSchgQSxNN6/TluGPoDI5N0hTUHQFUlSTagMi0P0ZZSOOliXzJT0mEbn7bHcV9sNIGTuWtcwtyBA4nq1dycUGQUhPEPi4/CO29TixsKguYm7LU7nagxZcQLXOEL8LEMDsaVBGK2F31i3HxEFmOxFNqY0tgLlnsuNeND0G1eI8cFKJBz3Vq9E4EqvV1FOWNJ4gWhPacfhiAHZWFT864d0cQUmkgLcgQN96ClcaRYXNcIIz05N/k/wAcUmQWmaKgVfjB+2p2GNIsNKy815fGtf2cVsL5XhDUt14kmpY774qa6KXNgDzk3O5HjhQt9VdtxxG4XwOK2rx3iqOX2iRQq3b3GClBWGVgvIVoDUHr+GK2ua7ZyfU4liNvh2xpbaMsAAKk8/2saWwt9WH4uW/gafwwrapDJAFNCeXhTamBIKysZYlgdupxQfNV9aJRzRKgdD0rim1BpFdmkBI8BTbFFuZ0oN6eO2GlJbEsTGsjtU+A2xW3Aw0JD/7FsCu9SEIsZQjfdvbGk2uAgJATmvh74qtZ4FAHx8j9rl0xWw4GBCA7MKfsjphQ4GGhIf8A2LYFd6kIRYyhG+7e2NJtcBASAnNfD3xVazwKAPj5H7XLpithWV4EhEVD6jd/5cU2FDlbBiKnbo/bCxsLkuOC7ICK15f2YF4nFleTkVYV6Co/pirUrqooVNa/aBH9MVJXFgwDNyZj4EBae+2FS0piMvAK7DvSn4DAtqq+gF4ICCN1JIFD74shSmkcwQkNQP1ah4/RjbGi0kU1VAKqwNACN/nimi0yMh4GZNuxwoI81vpyvU86gbEjw9sUOlV1CmtR79qYEkNB5eO1Pi6VGFd1p5h/jorDxGKCuJfmByAJ3O2Kt+vL4p447Lfm/wD/0oLql2Um8g/uYwbcPJ6kq1eolP2QPtD55h9ZuQAKBed+f2juPO2tSsxieS5L8AKUqNxTxy/FfCGMgL5pGbdQoX1aO3RT3ydrS4WLM3D1BzqCR1A98FpELbksZYmPxh1XuBXfxx4lMKUfq7sjNz79Nh+GG2PCViwOxCh/lXYffjaOFcbaQMVViXHYb742nhIcbWZTu3boCCfuxtPAVRIYuA/f0PcUO+NooLTbvtQOAxovvja8K30aHgSwetBXbFHCpNE/OlSw8RhtabMUnKnI+2KOFa8VCtHqW6k9vbFacYuLceZBOKCF7REqKMSD4HFNL4YJZCWBNF2PIbYksgLUhDXcPuOoHbG2NN+iw+It16MMFp4VU25VRVia9DjaeFayR7BnYU6mlaYrS1hHWpdgOganX6MKDTmhUAfveu9Kb42tea6OKMseU9Nq9Dv7Y2oHm06AdZDxPQ42tebfpKFUtL9rpitOaNR0ckU3H9cbRwuWNXB4HYe/TBaQFvCNdmJY9sUU3FDC70ZiPA0qDiUiK5rOTmF3FTtU42vCVRrTjuJCSNjTG0mCk0cVOJYl6/aPTG0UtYRV3qSegr0phRs3FDC70ZiPA0qDgKRFc1nJzC7ip2qcbXhKo1px3EhJGxpjaTBSaOKnEsS9ftHpjaKVohCyEcHbalVNd/HFNKXpxLKUZSy/zdD+OKBEWultFLDiRRvsiu5xtJiqtYRhRsWI6N0BxtPAFArCoYcSa9X7DFiQFS3aMHisavTbc0rX54pAXJFFHMGaJhXrvUA/RjaQKKrKkQkBZE5FgA1d6nptgtPCLUY3mSPirFSPsorbfPfCgWq1mEYeb4zsalt/kfbAlue4SSo9EIKfEqio+/GkmQQwb4XVW4rvx23p4fPCwVJUR1Sh5bAOdx/t4GRC2T1FiBLcVTolT+FcLFRQtM32TXxrWmKLtcgZgVK/EB8LE4p5rfi8F6U+nCx3f//Tg+uyHl5WcxqY4XdTI+xqZTSOMficxJ/VNvxD0j3ME84ieTzRqsnBWdp+TNWjMabChyzF9IbK8kmCSHiWTjGrfGoIO58fbJ2tFTubaMlfTuBxBqvDcgHEII81ROYkEH1hGZ9ldRTf6e+KR3Wsu4BGTHIOTp/eCm9fniCghDwulTRC9dgpGwwsQrkzuDx4oBtG6b0PgcDO1D6q6vz4AHoaGpr41w2w4fJEqJI1R+CPTcLsPngZ7recc0pL7V2FTRflii7Uq2xcLzAWM15MKjDTGwpkgyEkAgbjehP0eGFHVaz82YhAK/YXwxQd1QKkkfB6BhuoA3wJWpbrJRK/Ex6nfDaKtp7f0+RrvXiRja00hEbFlVqgdCaDfxGKQ1HGxQn4VFK+5r4YoAWoGKlAKcN6+GKF8cxADS8mjX7NOgxW1rMrsCB8Lb+1PCmKndc3HlRAajap6UPhink2WjSirv4sdziq0cg5k61O59vCmKq6/V+bqyemhFVNeQrgSK6rYPQMg50jH7JPfFRSnMIuRIIY1pxGwp74UGm+MINWIFf2dxXFaDc3D7KgKlOnU0+eBSsCRovJqlWNVphQq27UuC/IOwG6EEgjAyGxVpnmkjDSSKB0UKtNsUkkoYiJVHQKNwGBJwsTs6QoVAUBUI6D8cULQkaLyapVjVaYqq27UuC/IOwG6EEgjAyGxVpnmkjDSSKB0UKtNsUkkoYiJVHQKNwGBJwsTsrW8ETuHA4JQk8SQQcDKIUXRFLsSJSdvmOowsCG4qeujjkrfsmnSnbfFkOdoiR3kQmSZzvsmwAOBO6HIULXiVX2FST74WKtBGJyjsm1TWq9ae+BlEW4xqs3JJHiRzQDbY/LG14d2xExRmkkYhTQDiKkd6HFNFTBSNywVWWoBA3JHuO2LG6KrMSrHkoVWFUiA6/7LFJQkiMePdW7d8LEqjhlReUZBYABx0oPbFSFkQAjdl3Cnof4YqA08iNEfUkDEb0pviEHzU1SoJDFQd6jYUwoRCyo6gKoJHXvgZWu/d/74PWvT8MUv//Uh19BLNdeVFgiWZInl+tNcFQI19Uk+mp8R+1mHkkOKbfiHpDzfzhFbSea9XuYjItu1yRAW+1SnfLcZ9IZ11SVIIqj1QyRV+Jyd9+4AyxFIkJafCY04qlVWZX4lmPSoIwMhShNbNx5rDR0NQwPfCCxMfJSd5pGUmONAPiI5E8j74oJPcuFxMrBTEgc9BTYj3ONJs9QrGecciQEr0SPag9zgpNlDM4IKhmIb7RNQB9OGmJaVI3iNUq6dHqaEYbQIqn+6fgjJpuedDT5YE9FL0BIDJXg69iNj9HhjaOG3OrhtwoYCvwAHripUish6N8PUN2rhYkOCbhS1A/2jscU0ujMSqxVijdAKdR44rs40ZwpBHL9tthii7UxWp4122qaYVCqFjIjBlILGm4oF9zgZfF0yUL8GDxx7My7BjixK2NSVA41r9k77YqA0PXRjVQR3HamK7hc01TXZR+ynUfRjS274mP7sbL9rw/txS5nqevxf5PTGltcBMTVQood27U+nFLZEoYowWh3FRQ4rR5O9dih/cJWvxbkN9GK2e5c7yPRliNKVoADT23wJJcyuwq8TM56Feh+jFa7wvFs54ssfFK05N0B98bXhctwUuFVkDhdmVV3P+qcaUS3VY4mlEzrEpVDVk50ZAem2Ka8lKNXkiC/CePQLu3+yGKK2WMpcVMLctgvHcGm3TripHkvFs54ssfFK05N0B98bXhctwUuFVkDhdmVV3P+qcaUS3VY4mlEzrEpVDVk50ZAem2Ka8lKNXkiC/CePQLu3+yGKK2VIrhjQMoRFHHqQKn9WNJ4l91p80E0kbMjGEK0jKwbZwGG/sDgEllCih/Vl9VY4XMoBrVaEH2phpFnoiELS3MnMxQyca+mwPGg7V8cU7kqEYqxiZwORNIafiGxQB0XK86OUcUEVSw4gn6fHFNlMYdPs2jDhxcxTLzleI09FvBgd8juyAHvtZeQzpDGyRVgcURi3NWA8PfCCshslEhRDHy2LbkdBT3yTTaInIkYKsm7dFJ7e2AM5bqUsDIvI/smlR44WJHes9Z9x1alN/6Y0tqfqAMnxHlU8iNutMaY2uMSksoNB1O2+KSGiD6XCgUsfs/xxR0aRipHA8OPU9zit0iPrsn+/O9cHCGfE//V5zq5uLvUfLHqU/cpIIAAQzFpTXp9oDMSRFz97fD6R7mE+ayf8S6nx5UWWjUBIJA7HLMX0hO6VCSWgHqstduDAAU+nJ0mypzQF35B1LnYsWphDExWLDMi0JcMD9kNWv0YUUVzRRIpkR6sft9iMDLYKTASEO27dAeu2EMTu2x5UMztUfCqLgT71nFeVFrx9z+vCgrnAqKLy49q7fM4odJzAOxVn6bmgGKo7T4BwFxJUFfh9MmlQPBjtgLOI6oOaONjI8NVLGvAsCeP0YWJHVD/AL5SKEivQ02I8MWG7mRdmC0Hcd64U0iIw8qmQg8zsvagyKVrRklzJUhdivYnDa0s4yGhIqTSgHYYqrsIgnpkllbZadK+5xSabltpwI4wjFKclIHU/PBaTEqTNKATuo7gdDhRZVKho+oDkbU/VgT0UgECkGoK9AOgwsVVCXHx7qu6kbfqwMh5qB5da0HYV3+/CgWqo7yKeVOKilN6YEiy1JIlfi2KjiGPxVPzwgKStMhfZuLOgoDShHvjSLta4VYgwfkT9qjUOKCNl0XqoyOJTEy7xy12HtiosOa6cgxMwZXPKQmu58caXi6K1o0sdWjdVfwpUU+nAWQJC2dkMo9IkHu9QAT3wqTvss9XjIsiS8ZR9puh/DGkEtq8gkWRZGiYGqSk98V81rXTkGJmDK55SE13PjjSOLorWjSx1aN1V/ClRT6cBZAkLZ2Qyj0iQe71ABPfCpO+yz1eMiyJLxlH2m6H8MaQSuiuIuTq6u/L/KoCfHfEhRILJWkhYxqClaFl67EVpigkjZUtlblyVmVv2WUUpgLIXzbuG5hVCEgfakNak+OELI2ouUoFqxYH7W5GLElck3pSBmiDL4yVqa+NDim66IhZLm1pPH8PrCjrHX4h4dMCTY37191eRzEOsBteFXC8mdSQP2R0XCplfSlkUFYiaCVh1B3/AOBwWoGzmEcsZSOhmXb06dMU1YQ8hmDcWYF9tqbYWBNKkZiLgsfjG8hHSpxSCsujAoQpXuSpG/34qaU4acwxrxOxB6j5VwsRzRU0URqHBUHoT1ORDOQUZIkLrybiQNq+GGyghd6UP8w+/vir/9aAa2s0DeWlhi9SWZ2QtyCkIJCfvzEkPVNyMZPCPcwvzbJJH5l1YtsFuPht1IY0p3GWY/pDJKponkYcFViycg1fh2yayFoaGMA05o1ezCoJ+eEsQooshZuJpuSwHRh7YUNShTCaSciTUpTYfLFBU6ADkCwXvX+GKG09QL+8BMYNQoIB+nFVld2FACd1I3Hyriq8zSEbUFftGg/VitlTd5TUs4IPjuNumFBVJdRuJIhDIyyxrvUilfY4AEmZKiZpQtCRQ9Fp2PhhY20biegHLYbBew+WKklG2s37irqjPFUsrdT4VwENkDsugnjkjMjsqsTRmJ3Hy8RgKxIK1pY46u9WH8qtWvucV2DUcsZn5SUU9TU7UPgBhQCL3XCdubRcwtdiT9int4HBSeLd0tzLbwtFFIXr9p+ZI+44gJMiOSFErtGFc1oa0OxOFh0RMnNkUqaqNiBvvgZlSmZuA9Xp2VBQ4WJ81goOJViU6FgKU9sVttzxZiX4/wAqkVripaeWR1Gyog6Be+KklpmYKFJDPXZQOn+tii3EyEGoWvamKlf67ha+nGFpuvcnxxpNmnLcfYAVSF3ZXPwjGl4nCVi/L0kYHp1NfoxpbXiVwzRtsx2AHwjGltwkk3RqBz9lBv8Aj2xUNRzwrs6KHX7BrtX3xpQQ3JdFpB8CfDuyk1QY0kyWCVi/L0kYHp1NfoxpFrxK4Zo22Y7AD4RjS24SSbo1A5+yg3/HtioajnhXZ0UOv2DXavvjSghcHjmkcyiOOgqN+/tim75rWlleTlGwZRTjUVJxRZPJzSuslJDyr1G1BXxpip82hIyNQvWooAuw+nGlty3IifhIFde1B0PjjSiVLluucwMzrwpTkF/DGk8VluOVHkoCEh47FvEd8UA2i53sVtwF+N2FSV3B22+WBmTFQDQJGI2lrETUAGjD2OLHZTnuokpHA1I3+2CN6j/Kw0iUlM8QSTIOXfvWvhjSF4kjKHnHTlsGGKb71HalKlj+1XvihawVRXl8fQCvTChpZAAObcm6jAi1dLhSNjyPViep9saZ21W1/wB8+/XGlsdz/9fnmrfUZtT8pRywiS2i9V5owxQmQSmjct65jT2Mm7H9IYT5tUHzlrBgJZZJqxlRuV47qAaZZj+kJAopEsTCIsvIIGPJCenzyaKUmjUqxFCo3O4p+vFaWcRUfbKnoBSmKKCvJbOIlZRsRvSm3+tvgBZGKFeKQMo2Ffs1phthS70rlTQrsf2uuNrwlv0pwCFTp1G1PwxtNFbFBLO4VEUt4DDaOG131GdiSoWincHbcYLTwKPps5pxNR1FMLGm/q0pUttReuKeErViDbBvi8CD/DG0UuaF0WhPE98bUhUhhdzxQ1HsAae+BkIrx66c1hcnf4vs0riu/RSj9VZSxNJOlaV64SjcFVkNy8dS1VApuBgU2Vss0hiRTIpQbAcCP4YVJUuUlK1B+YxVoPNuQxHiBXFd2vUlJrUnsK1xRZbR5fsVoqnpvscBWy5mlO5ap6DbqMK7thZAQOXXw3ocbWm5FnSpZvme/wBOKSHL6xTkrd9hSu/ttihaTcVFQa/LFd1wE1Cqnkf2lp0+eBNODyouzkdjQdPpwqtCyk1BJB70xRRXrG5puxJ7hd64LTSx45A3IgiuwJFMK0uUTBeK77brTp88C04PKi7OR2NB0+nCq0LKTUEkHvTFFFesbmm7EnuF3rgtNLHjkDciCK7AkUwrS5YmoCzUrtSnT542tOpJuatU9waA4rS0RMaFaj5nfFHCvWFulCd+lRgtNNSQOCT2G1K1P4Y2vCuSBuAc8iP2SOlPnhtaaEYqWarKOhPT7sVoIhoTGEQVHH4wK7HBbKqaLugKvFuftfCDXFSsIKoR6XEnpyH6sUK7h/q3NowFNAABTFNbIf8AecaFeIHRsUWqRxM0Y5OAWOx27YrSm6yMzLx+Z44oolwR1KlxVfHj+OK0V9WCsVQmvSTjQU9sUtcpf99t08MWXE//0IZdW15InliJrWP0IzM6M1FkkcyEVVvb+XMTJzk3YtwHnfnCCvmXVYjLxninqpBpxNPHtluM+kMyAUuV3Fv6Rtw5UfCeVVqe9ckyHLkpGOYIa8aEdW8fYYoIKHm+EhRRW7UFST7jCxPNbdXEjqsQBWm7SV2PzxARKSwIojLg8vE4UVs3BMqqY/WdU6nkNjioPS2kWo5JIwpWhofvxUBcsqBSkbNGpO1F3rilfIWNTWRTT4n/ALMCVFUIoxJK16HYfThYhpp5C1VRW41qBuDjSmRUvhVQCzEVrVe2Fiu5qNwpCj7Lda/PFbWhm5MxZkZuoXpTFW1VCtSeXhTYYFXI6kUcsafYB9vHCoV4pbRODM4PKtV414/6+BkCFGZ+T8jR0H2QBQfdixLuVVqGqOm+Kb2cULU3oCd/DFVhDDlw29+xwoajDAVBqf5gK09sCgLjvyBNK/snbriqz0HB+Eb7fPCjhVZxyp0cgUO+/wBOBkVihgGNaVH7PXFDjG9KM3Tc1OFWkQ1P8o/aG1cUBUUoGPI7U+EdxgZNg26EHkWIFSh64rs5eEjisnHl+0KgD5jFI3XOYIyYvthvtMamnyxU1yU5IiszLWqCgDg9RTFjTalAx5HanwjuMUtg26EHkWIFSh64rs5eEjisnHl+0KgD5jFI3XOYIyYvthvtMamnyxU1yVnDLGsZ4yxr0IO4HauKa2Q9VVwxBAGxHUD5nCxdygUqeBDncDqMC8lrPE7AuCATuOlB7Yrseaq5jhIECElh8RI3IwJNDkqDiI+MLgo1C0TA0B74UgbLRJx+H00JrWtdxii1zGNkYSFlcn92u1B/XFQUKJGFQ3OlMWNrWaYlWJZlOwBw0i0Qy3PEyUYquwXqB74GRtDoLpq8STy+19OFiLWMrBh8NG9jihVCyKwPpmh36nfAy3WNFIV5hStD0rhQUQsErgMKAqKqOW2BlSl6d74P9qvXDYRUn//RhvKaNNBkuJJpI3MqW1KGOKRpCPst9r5r9nMTJ9Um3FyDzXzNZ3C+ZdVD1dknKcpAASabltt8ugdgzEeqBMMkKKHmKxkfZAFP9rCyquqhIO8UjIzfa5Lt+OFiVGVeDDlLzY+FCa+5GLEqTIPTNJARXetf1YbQWloCfj6dCOg+WKtoGNaPXx6YrVr2aZQIvVXiTUDbbFNnksLEMKtyPZlApiguJPEksefbocVWrGzqWBL9jXff5YUDdogigJIp0psMCXVq1Fbb9of0xW10IVGo3zp3xRycrNU+B6g/1xS3yjJPOqrXYjoMVtYwAPwkcj1psaYUNUcsAq0JP2sCu9RzVGG4Nev9mNLa6KLm1VH9MJSAv5RmtSodfsjAuyGkkBUgd/AYWBXwvRTStB1Ap1wMgrMGNHPwqNgT4/TikqEtWbkzbdAfH5YUK0ckdH2YkrQlxUfRgTbSBFpt0G58cVGzRQEGgpy2I7fjiqyvFSOXegwod6laKDttWvXFbRTodySvFujkCo+7AzpDoCGDFgSDWpHbFiNlRmkeYlCDXcigrinmpyHiWFSu4oPDbEMSt9StFB22rXrhW0U6Hckrxbo5AqPuwM6Q6AhgxYEg1qR2xYjZUZpHmJQg13IoK4p5qgYxUKFOv2WFSK4rdIaWT4uKn3qB1wsSiNggbnRaUKk1/XgZdFDiASQxNdxX2xQqOzSyLRyCRQE98U3aqwkhBJZo5BSu1R899sUnZYZBUMwZ1PUkd8Ue9aBykDKD6dRvTah8K4oRE7RdI3EpH2SooD7b4GZKHpKWALCp/ZbYA/RhYL4lkFQ4eqCpKsDTFICnGJPUaRO561pUYlAVomje4AZRHH1DUqK++BkOfkqzyQsD6fN/EstAD7YpJQpR+QJaj96fEAMLAr4rdjJwostfi6kbYpEd2+R/303Xj1PXwxRb/9KAwtdSXmnTG6cQLA4tgFLJC4kNS38tfHMOfM+9yIfT8GC6/aTSa/qrTTNIwl5SSM2z1HUZdA+kJiEst7RW4y+q3BfhRZGG/wDqjJkqIqlyhjLIErIuwbqBXAGUkFIDuCi8x9pl6j6Mk1lReNAiEyVqd0A+L6cUUHGNFYchXxp2GK0ukjH2VUhTuD3+/G0ELZIw3FQhUAbL1Pzripbi5EUZSQKhSP8APriUhTYRKaAFgNz/AE+eKDS12ZaUPpr1Wo3xQSuaVWccjSo3qO+KVFuJYHZVHcYUWv5HiTQkHoP6YqreiBbiWMFhSvTYHvvgTSlGFZhUcz1A6fPFAXM1QBxKgdCOv04pLXJKBirV6EA7fRihZWJjxqQex/hhQqIyqCzA1BoKGmBkCseU7tRSTtxGFFrUPFiCKt2HhiraMxPwL8XfwxUFc8jUbbb+Q9AcVJaKELUty22HvgWlWCVeDMCyyU22qKYpBcicE5NQ1/XitKfFiTXp8t8UODbBSlB7dMK2u5RE/Zoevw7kjFV0ckLVMiEN/KOgwJtTJ4kn7S969foxpCrC7euOC7sPgYbkUxLIHdu8dzcMJI+JNK06VoMQsjvup8oifs0PX4dyRhYro5IWqZEIb+UdBgTamTxJP2l716/RjSFWF29ccF3YfAw3IpiWQO6I1G4DRhJLfhOtCHU/Ca+OAJyS70KzkEI1GJ7jav04WLccjc/3kXI/sjwH8cVs2sf7RZar4g/qxQujeRpIyijc049PxxSCSjbyW6jtRDLGskbD4GNagj3H2sApnMkCihFmeBAOPN3G5NajCWANDZvi/B+Y3ArQdCKdaYppXk9RoiVWipWtKffgZKUitxXhIvE9TsfvOLEhFqPTV+BBkCipFP1V3wNnJBSLyIBNAfiFK1od+2SayFaFIzMq+qwMgpRhQfOuBmOfNcTUNEJV+AkA8qcjijyUKK0QEbnkPtqO39cUc0VA0ShUWTkeJqSeh+7FmKUvVn/yutPtfjix4i//0+ez6tc6bp+gyafLP6LSendxjj9XYSueSSVPPl/Lx5LmJVmTcDsHn/mu9i/xJqfpkNEs3FAK7geIr2y/GPSE8VJWb/iEBHQk1ILD9e2SpBm0dQn5lxsKUAFe/wAzjSmZUfrXxqoUEd61740jiWvOOfJa+FTu1PfDSLb5LTkpAJGyEbjArRclRzK8uo7YquW4ZATQMfCtaY0m3LeOFI4gV60FCPljSOJYZ18K0+xQnrhpSWnn5vV1FBsa74FtbzVWJXYdAaf1woa9YGhYVONLbhK9OQoBXpituS4kAK+oVVtyo23xpHEt5nxO3TfFK4XFB0/HGltb6oJqRXag8MVtyyUO/jWvhii1QyhviaoI6E4ptYzHmH2FfDFWjL/KAD/MMaW10czCg6e+NKCqPMAACeR8BiklYrKRSv3/AKsUNhxXjQMG6CtKYra9J+KgSBSOwI6YKZWt9f7RY9T0xpFuaUMBQb7UPYYaW3esvqVK8R2p440tqqzUJY0p3au+Ck2sZ1ZgBUg/ZPTFBLaTgXCem5Wm3LoPuxpIO7d3P6kpJ+JtvjPsMQFlJT9ZfUqV4jtTxw0i1VZqEsaU7tXfBSbWM6swAqQfsnpigltJwLhPTcrTbl0H3Y0kHdVuLiR50cMZWGwRum2ICyO9oeWZhJVqGnUUocKCd16zb8gQABuTucaW1ryq1Ph2O3xeONKSteZOSqOinrX9WIC2Fa4mV+DKxJU0ViTTAAmRWyc+XJ92I+I16fKmFBbEjcuJYE9FZt6A9caW0S0kMcZhmgCyV+GZaigPbItmw2K1JoQGjlTgKbNGu58KjFb6KlqHjpKsPqBGBkRh8JUdq9d8SkW6a+hZpJFhWGZ5GeJQDRAT9n3AxpBkFGAwvI0k4ZkbrwBoT4UwoFdVYzSWwaMxo8cgBCkCqD2wc0k0ppcNGVDqXgPRdgfoOK3XuXQBm5+mQhAqCWGwG9CO+FQmf+KtY/ktPs8P7pfs/wA2Q8MebZ40v6L/AP/U5xaR2sCaNNJbC+vOEwhLyFUtyJCAzJ0f2zEmd5DzbwNg8+13TTF5h1KFJAxjl+NulSw5GmXxOwRwWlslsoowc16UyVqYqZsySGBJBPxHpQY2xMFktsqGvqVBO2G1MaaECkVDEj23xQIuEJYfa+Idu4GNp4dmjCx3FK+GNopcI/8AfiU8KbY2tNNTupAA+1iq302qCFrXofbFWjGa7jfw6YqQ7iwWm23icUNMgqvv2BxSQ5UdhxB+jFAi0YiOu2NrThF3Y0B6HG1poqvauK04qtaA1xWmwgIqW/28VptIwSORoMbWlSWEgjbb23xSQs9EdR8SjqcUANJEajxO4xWkQ9m5HSlMFsuEqAhO9ajwwopt441ApWp8e2K0tpGWFa8fHviinFI/5tsVIXxhY23o1eg7HFIamVa7KVqd69MUUuIjCbGpA2GNpprgrU3P8R9GKtNCEcftA/RitLl4o+42PRScVC2ZVrspWp3r0xRS4iMJsakDYY2mmuCtTc/xH0Yq00IRx+0D9GK0qrEp4lgwRTvx3OC00tuIyO3LfZz1IxCCHDgqjY1HSteuFaWhQVFV+L374q6WFUoRWtdwcVIVVhLfDwBK78BscCaVfQRayGNhEw26nifoxtPDShEisykLxJb4X6jr3GKAFdlBLSc+bHYg16+3bFke9ctvcOysWULSnqU2HtjaeElYtu6OVrzjrRwG3/DG0cNNOkBuGIm47njXqPbHdBAvmqxyIFbiSaDqdhgZWs/dNykDFmXq2xGFDo4eY5iYBV+3uNvlja1fVo28Rkb0ZA9NzXeuK0OiM5Rf74Xpw+nI/Fssdz//1efR2Yn0ny9P6Zae5EiS0NY0i9Q1aWn2D4VzCJ3l73Iidg8684IYPNGpxKikLMACGr0XqDmRj+kNZtKTJI1eEK8SKE1qPxyabPcsIkK09EALsd8UH3NUmUFmh2/Viu7i0gFOFK9q4V3c/rAL8IWvgevzxSbcvqvQBRVfA0wI3bEk4DAoGDdsUklotLQgx/PCxsrQJwTRNh2xXda4ZjXjSmIVr94BxC96+OKrTycmg9zihoMwIpviq/k7Ntv7dsUtc3Le3SmNI3cK7juOnicUtnmKfBQnGkbtb1IH3YpbUuNlrQ74qqFpiSrHb32pimytEc1B4H7O+2KF8Mc7PwUKCexNMbTRXXJl5BZCqlOgGAJNqVZ3qG3p1rhYhoo3GpAHhiq3ga0BGKupKtDSlemK7thDyP7Q7+OKt8n5UZtl+zXAq4s4VaLU9yRikrnExHKoWm/hipBWMXajMeJBwo582iGMhNOQPUHr88VdyflRm2X7NcCrizhVotT3JGKSucTEcqhab+GKkFYxdqMx4kHCjnzV7czF29IVfqQ1dx44EjyQ7sxkJd+QPU/LFBX/AL4JsKEmtTik7Bc0cjJWR6Adaj8MbUhSIYr8bbDphRurQI5dRH8BP2GNPxJwJAcxkDMVkapNGYD4T9GK+50QAcrzZ16Egbb4pDQmT7PH932UV298KOJTWcqCtB6Z6rvQ40gSVIroxq6x8QGNeVCafLGkiTQmTqyM2/UbV+nAUgtfWD6nLiVPZe3yphpHEuEzlgQm38lNsV4ipGVxISKj2piiyqR3EylmSq8h8RAGNJ4i163+VJ/b447I+b//1udWUsUT6TBQVRpXMQ5UuY2cji6jqVzBkN5e9yI8gwPzNFIfMGplYFSs3wKpqFAHQV3zIgfSFopO0TN9o8VHRQaVOTRS0wJTrXwBamFFNgsh4irSEUoWqMVtpgykD7LDxX+OKKXxKRLzkKPHSuLLqt6ykgKAfsqvbFHVtizuefFCNtumBLlVy1WHJfnvTxGFG6+BSzMFWqdKg9Pc4CmIUepfj8RU05E9vbCxcedN6VJ2YdxiqjyVahRQ1pv0OFi1IF34qfmMVa4/DSu/YUxtVyhAtf2qdMUtqE7AkgdcVbh2ehVj/L4HAgOovxVG6+B3+WFLbSdgaVH3YraugdrZnRlNetSK0HtgZb1shk6VBptupNRTFiEWkD/A60ofaoOC2ddVspNGJRGK/ZNRhQSh1lc1oN/nhpFldR1iBZTxZqgE7bYF5Nrxpzeq7fCB44qskaduGw4j9kYUbtnkGAUDl3PtgVaysrj4Qa9O+4xVeXLkB1APWqmm+KSbdJ63BTQFT49jiptayOpFaENigu3UgLRm6Ft9xirTKyuPhBr077jFV5cuQHUA9aqab4pJt0nrcFNAVPj2OKm1rI6kVoQ2KCuDgIY+XTowrv7VxVZ8bKHFD2KgYqvLv9mQhgDvUf0xTbmVxHVXC+K+2KFrKVVXDV8emKltGVQVBJjb7VAOuKtIWAJZmoNgo6YqFVWqW9Ko+Gj8hsQfDFNrPhB/bO23GlDirmt/g5hSDWhQn4j74qqvAOCmNuYpuKdPnimlEQhiU3261Pf2xRS5LdqgMtP8on+OKgOC/EAqF6e5BwK2bf4fiVY5P2F68vnhtJC/04/SVQeEoO6MdgfbFVP0f8oePUdcKv8A/9eDXBWG78jWaUaa5ha4lkjTlIFZzVaj9nMIi+L3t46e55550ljXzlrHpyAxC4PB1Wp+EUGX4/pCbSNnRQrSKPUc7v128csQVMPbksApf/Kp9rFiCFyGFU4gEMfiFR1xSCFpEsv2XLH5d8Uc2pElKci61GxFKUxCkFbwavxFTQV8DjaKptVAo5FanYncfTinZvhsS7HkTuF7YrTRD1brRtiAafLCgqfpGgUqajoBjaKb4t0DEHoK98U00RINlJJPbtihUWGThv8ACx6MemBNLfToRUgU/X7YVprg/wBlqH/KwKWxyoAq7dt6HFQ1uG+H4j39/bCrjQ/FGvE9Ca9/YYq18VAGSrdq9cULVX4t1qew7V98VpUWOpr0buK0GNqiebiAyKV4V4ni1CPbAzvZDsrn4HrxG603G+Fi16bBixalO9O2BaaNC3JX5EjZQd8KFvBuXU0O5bFNL1Eh3Dmo+yTgXdsCRSDUE9+Pc4quVXYHieDA7L2GKWpFZAGKgIetR38cVOyqPSKAshp7AmuKdlC4Qq4B6Hcb9vfEMS5A60P4Dr7YqvVXYHieDA7L2GKWpFZAGKgIetR38cVOyqPSKAshp7AmuKdlC4Qq4B6Hcb9vfEMS3EhG7n4B26mntikLlQMTyqB0VsVbZJBGHUggeG9cU9FSHk0XIRFlG+3anjio9yjcqCAy0+Lfj3GIQVsaftltuwHf2phQFxDNKygUWlePfAqoSpC0ornZgeg+eBPRq2lf1QQygMKVI/hhITEu5t9YLBwXB+FSOvtip5qrejJVm4RtQ7bg18KYhJood2oQAxr0AHSmFja90O45sABQIR+rApCnE781bkwoaAkVOJWJXzMhnPJmr2OKSrmVWp67dNqha/dTGk33qfBf5P8AhO334of/0OYySXthbeSb8TtDdTyOkM0fVU504EH9k5ibEyDkA8mKecg/+LdaVlWNY7jkyrtRiN6eNctx/SEWkbu9CRGrsw3Ht8vHJramtFiWjb09uuFALnuK0BjK8B0B2r41wUniWs/IKQrx16sKUxpFrjwqq8vjG9T0r88KVPlxDIxBDd6VI+nFja5AtRSQV6VpsfvxSF6BKMDRj0IB/HFW0Mbj0uQLD9o/8RwFQhmIVmHvQ/PChsyRjiBXl0LEbDFFogxMK0YcaeHfFlSiJLg7M/TtTbFFlTq5B5gMq+2+LFoM5NABTsD2phVsyMTUKDTFNtguCG4Ag9KeOBNtPTtQtWhHb6MWJUwxDVqSx2bCtr12qp7/AGT7+GBW/TPxEnc9cKVygNxPHjTb5nxxULDQNRiykdO/04quahUqCSg+4nApUwh9MsRsDsR1rhRS5G2oxPj164pXLGrCvb3OBacFRSNya7hVxWmwpbuxA6D2+jFLgQzUDNxAoQa0xUKhumaIRLVUXrQfaxpJkSFrBQCQwZD0riELBwqDU79FU/RiimwpbuxA6D2+jFLgQzUDNxAoQa0xUKhumaIRLVUXrQfaxpJkSFrBQCQwZD0riELFljDKBUU79QMaRa4RKzFQS/fr3xUBrgC4ARlK9Tiq/wBaQoYlBEdajscU2eTVY6clJUDZh2riqmJ1BFFBINajCi1aMI6n06knqx64EhqVSI6g8h0bb8MVLSEk/BSo6DviqpbxRvI3qEAVpyHUE+OJZAd6tfW81uUZuJQ9JAKgYAsxSE9Is/IPyqa0/HvhYIgerIP3ZXiP92DrgZ2h6nlQkqT0r+vCxtVi4esQx5ACpTufliUjmiLuFEgWWGcPGDutRVR+vAEyArZCeofGTpXt08cNMb8n/9HnWrC6EHku2nCgWyvIjMKeqFk2Ke698xKoycgGwxfzRFHqHmrXZ7g+nL64cRx05U4079stx7RDICwxOeQuxEStwSoDVFcsazupxqKbhqHtjaAF7xMjdSfbFSGkeYVG3HwfsfbFNlrmCPTPH5AdcULeKinKgTse9cUdV3JF6MK9gehxSSFzSxEspUID1YGpqe4xpbDv3CRMFo69OQNCT40xTsFBQHACpU+Nd8WK5oWoDSoPUd8U0skqN6GlacsKC4S8v5gOwGKAVRI+a8gxp49MDIBZyT4qVJPSo2wo2XCjVoKqo+IgbjArTzBkC12XpTDS25mjCKa1Y/a2pTFbbYIwVkHADZgf14FoLfSJFB9w3xWnKp3NSwpSmFFOWOV2Arv+zXFNLv3kTFKipFTtWmBI2WKSR1qteorhYuIKluO9Nx7YqsDEinEUO5p1xVW5EpsoI/ZNaGnhim9lwUIpU7lhWq9B7YFaUFF5ISK9VGKRsvd0c1V/TAHxVFK40pKySSLYBhWlGp0OKkhSdhsooVHQDCxKpEOMfEg1b8N+mBIbUFF5ISK9VGKRsvd0c1V/TAHxVFK40pKySSLYBhWlGp0OKkhSdhsooVHQDCxLQiYJU0Wppv1xWlWOMcSFoXG1V8MCQFQuHVVdWTj3BrXGkkqbzR8FUcjTcNShxpHEFNnBoFNQe1O+FFtrA/psx6HwFa4ppsFI2Clz8P7NKdcUcmzLT4o3PI1BBxpbbcNXbr/NgtJCtaiAXJ9TlQjsNj88TyZRq93TJC1yUR2EanlR6gdOlMQg81I8WlIrVTvxHvhR1XIkQYipG2y13wJoLXU7qOvevXCpXR+mk8fqJzoNxUA4FHNVvfq73C/AY0IHJzTt4UwBZ1bXrJ4N1p07eOFNv//Sg+s2+mG58gepduiXaSxshAZEJkoKnqORzDkTc24cg8589xCDznqtq7uq21x6YpuKgePhl2L6QsjulN0LIOrxGqNRTHxIp4k5OyppYDacySzBhsN9sV2UWdQWC9+r9cLFTL8aLTlvtXemFFqgZKVpxIOy1wJta0qq1F+Knj0GKktNJWrHdm7H+GKrSUUlipJ8NqYUWtMkbfsUxVpigC8dvFsVRaSjjz+zQUY96YGQkovNGfg3KdRhRbUboBRajvXFFuJjIPxsa7kVxSQG1au/qFT2LUwK5yyKeL7t9qh6jFbWBS1CWAHQ4UKhSMoeT1foFPTAlYCpAqxLnbbbb3woVIwyqUSWhPXAydzkNFWQUHj44oBXcJWX4JFBO9DQEU98U0aWGSZd+SknuMUWVq0L8mIXwUdDhQ568mIoFp8QwBkWoWQmjD4QOlcLELuYAoopXahIJA9sVadXVygNFPfxGKlEpH6URkdwqnbj3I8cizGykGiKtRif5eXfCjZ1YGb4qdNwo2xXZY/pEkxdtlr1OKDSwepsCeNQKn26YUItI/SiMjuFU7ce5HjkWY2Ug0RVqMT/AC8u+FGzqwM3xU6bhRtiuyx/SJJi7bLXqcUGkXaXhSGNHiibhUlnPxEeHzxLOMtlMNbfHMH9MVJjirVt/HFiCOakJrcsxAIHue+KLDvXjPHmeRG1egxpNhp5YXPwDiANvn44oJCtaXc9vH6ZkWNWbkapyPzxLKMiAo3Fwss7TFVUkdF7+++GmJNm1NXSoHHruP8AW7YodzdW5UJPWvv44q1DLKJS4HJj1riokVVizyjkrPQV26jFKjKGB5DoxqPHfxxYqgEgQnptuf4Yp3W0cEGlT798VWqZVlDV4t2JxXdWpLI6UAdj1HbFO66r/wC+W+1w+0evhgXd/9Pn+q6hcRWHk8CBPrKljHE68hOnqfbj/ldcw69Um+9gwHzzcyP5y1h2iPGWerL1IbiKb5fiHpCCSkbPM2zFUSoRm2rk0WVsrQxEwtESVPXrtipNbU19ZQDZGr0oRivEtLQsTy5Bu4AxY2sUwCoI3PQtsBhWwuX6qHPNSdqIB0J+eKdl/GAVJWpp8IUVFfAntgTstjjhH96pr028cJQB3tmOAV5RkVFV3/DAtBZwiIJ4kfyr44V2Vo7RGHMAEMNkJpv7YLZCIU2hVCzEAgdq4WNLFWPc8D8q0xQvKQ1BjQse9egxTQWAxhvsbj7TdhjzQ2wipyrv3NOp9sU7NIkbEE0G9OOKgLjAqlgy8gfslemNrSkQAOLd+h8B8sUKpjUbAACn2jvgTQaCQ7Bt/fpU4VpzxnsgC9ieuKCFMIgrU79CPf2xQqRwqVrQk14nY7e+KQGpY7daqjcqdGodz3xSQERHbW7RqXljVT33+nAkAdVX6nBUrD+/TYK4GxJwWy4R0bntWEALxkMh+JSKUw2pjshOCctl/wBYN2xYUsIh4n4K099sKtAwE0oaYocHjUNRT1+E0xW0YYvUtVlZakE1NKd8FszHa0PwTlsv+sG7YsaWEQ8T8Fae+2FWgYCaUNMUODxqGop6/CaYra5nD8iyjkN99icCVwAAXYKx3r1FMUBaXSp+FSffbFK0SLUAp8wO+FDYkAZqJtTpX8cVb9R9ufQj4Sf1YptekSsnKTqegG2BIHe5EjSVV+0ag0bxr0xRSzkiuOT/AEj+NcKGnKGYE7g+GKq8RQCvP0nX3G4wMgUPI0LyHgCBU/dixbYqVBZyAe1KnFLi8S0oSyjp2wodJIrMpALDFSVVGh+IS/CvUEVJB96YE33r/rcf8reHX/humNMuJ//UgOrymKT8vbZi3pTI0iFyNn9Wh4nsGzEreTcDyYN5+VI/PeuRGJiDc7ry2+yDluP6Qlj7qfSIVPhbonhTLEKtmsMzLbo31dv2mlANDTqDgKY77ckHIrJI6hwzI1C3UH3wsCKaZpqdVIG4oOuK7uUiSvMKidPckdsV5tFl5g8aAdh2wrsvTizegHAQ/Fv/ADYEiuTkLRVbkvpg0Hck4ryaahLVpwI2xStUoqByvx9FHthYjZtdiGcURj0HjgSF0r8iRw2XcnxxUujHNGfiAV+0T0pitKSOwcshFOtfDCxC8kuOTDatSehPzxS7knEs4op+yMVvva9Zl2OyUogp0xW1oDMKJsB1Fe/jirihoQKMT1PeuKHKSoHQn38PDAoVH4kClOnQ9QcWRU1rWvCgHQYUNBOTVBKkCpr3+WKCLVYVWSFgOXMkUFaj/ZDAkCwrfU51jfmiqV6Idzv3FMbTwkNLZSKhqoC0qa9cbXhagkC0T0/iSpRg1MVB6Nm6mdXMimTl0qeh98aTxId2anxCh6Up/HCxbVWYeoI6r0r74qscoHpQjx7YoLmr0VftdupxSiEllSIo6l/5QduJ98CQSAouzU+IUPSlP44UNqrMPUEdV6V98VWOUD0oR49sUFzV6Kv2u3U4pVP5UYD1D1LdcUOlDrWpJA6EdMDItRrK4JAX4Oob+mFVspO1VrXq3jigtNy2AAUn9kYqqEPCoRxxZt/HApsKvpjYqSwU9adMU0vIXg0nIApVgGHcdKYp6IeQRcaDjXv44oNKewHw/s9fH6MLFWURJEajmT9gbfCT3bAyUSF5n4+vU+IxQqNHC1PTIf2JoaYpPk28iBONeJPWortipKnsBQfD/HChWhZUUuF5NTdDWg/ysCgqXqr/AL8P9v8ATFbf/9WFavdWdjB+XNWHCdGnumlTnypLTgn8vtmJw7yLcDyDzzz3PD/jfXXt7d+El1yiEn2lBH2TluL6QnkUklFEViuz7le9ffJqQh3jZVLFgJeoIJOFjSpyhkQ+pJGZXFC24Py6dcDKwfeosSpP2WK7Ajt8sLElazojhhFUru1e9fliiwOjmYhi4Xh3UEgihwra6GWL1g8rjj0JVRU/IYKSJC11xLalwI/jj6GRlofoGISSHEw0jkVlKAgBD9v+wYoXywkPzaH1FYfuyrCg+dMUlFRvai0WjKZif3sLjjx/z8cDMEUgJVQzmOMhV7DlWp9jhazzakieN/T4u7AVam+5+WKmO7RoYwKbr0oO+FCxi7ACpO/hTFF26RatRjUAbnwOKkNSSu9FKhQNqgfrxW2+dPssNtiP4jFbXxcy3EMGL/snr9+BLfpvQoaV6fIe+Kad6TRAiisT3rvTFHJax+FJASSTTj2oPHCrm5o4K1avUjw8MUcnRmUNzhJTehPf7sUjyXqt9Sqv8VaAchXfAndc9rdqGLMOm78vwxtTEqBElKdajFAcDvQHin7QruQMKr2bkRxYEDf5YElpmT0zwJFTuQev0YUFYQykEtyoPnihzyoGDoSG61xUld6ju1XbZvtnpUfLAm+9czciOLAgb/LFJaZk9M8CRU7kHr9GFBWEMpBLcqD54oc8qBg6EhutcVJbJZ2MkpJr0YYE3fNyupRVL0bvXpTCoOy4yxspXqfHufpxCk2p8QKcTU9aYopt3BUDh8QxUly8nPNquvTc74FCvaQ1q3IoK7A1piWUQjLiONLerPQOCsUdQ3xHxOBmaQjwFirBBUDc17DCwpSkYCQMq7+9N8UHm5SXUqrVB3ZaDFebe3FWKkhO4xVxoGAJFT0223xTSt6JdFA47dD4fPFNKEzEECgBXEMS6MtI1Knkdiamh9sK82vSHv149Rgtaf/W57rU3E/loXHOP023kWq/3v7IzFG/E3XyYX+Yb08+6/HIHd3utlbY04ilfDLcX0hSdykLzsyMg4sEG4J3r7ZJNlCuX4K1aKtAQD0wsCnlrPA8SLLccoU+OGPipcv/ACmmQIb40ls0cTzu6sFkapK9QT/KD2OTatrQpK9D+7I/ZPUYoLRicxF2/wBifEYUUtjWNXUB1BPViKjFWxIrEM7dNh7/ADxpVhLEknenftTFBVeDy1IkCItOKtsaeIGKea+JpSXHNmRhRiBuadK+2BNtRlkIIUKy7sXHwn5YrZV5LiWYBg6IlKFFJBY++NJJJUebtxRIlCnblXv88UE2uZSjVdFSn2lJ3J8QPfFPvQ8j16E0rWlNjhYNAsygVPJjua9B8sVbozDiKAp4jritW3xfilVVg5PwjrtilejcR9gL+OBVjMA/Jgajb5YVt37uTjGlEUdWYkVPjvgXnsiERowJndS6EAUPwgDG2QHVT/dSSvJMOAf4l4nFFXzbkgt/hQOq1FeYNd/A4VoOW1WWEsD9jap6bYLXhsIYxSgVYU8B3+7CwppgVFOPxGnypiraQSsaBCa9dsbSA16bo/FlIJ2xRycy8dxUKPHxxSuACGr717Yqtag2UVJOx9sVbSCVjQITXrtjagNem6PxZSCdsUcnMvHcVCjx8cUrgAhq+9e2KrSzK3wt8PbwxQqCKWUiQCgPfYDbbBbIbrXt5IyaUYD7RG9K+OG0EUtZF/Zpt1oa1xUALgvEB6VPX2xVa/xNVa+4HbFBVFnm4hDUx9CPHAkFdJBIqB5KRsCCoJ3p442kxRMoWJUCyFmah4DpTAzOyGkAdyygD/Iwhgd16/CNhwCipPfftilajsooqH3r0rii25DyAQx8T/MD3xSVVvQWBQKtKdjQ0A+YxSaAUpas3x/Cyjbw+/EMS2gIAXYA7tTqaeGKVnGT/fZ68vo/rhYv/9eBa097bP8AlnEZwGaJnDsmwX1acOh5ZigC5N18mDefw7/mFrzU5gXR5OdiBx98thtEIAssdeJkuPTSRSacgKVoPfJpIorZJZCCq8WJ6gLSo8cUErIpY4SrRVaQgg12K/6pxQCByb9SVhuF5L+0ehr3/wBbFNlQ2XcVZq/F3FMLEIqC+hit5o+JLSH4dtvp+WCmcZ0ChxJGFIIDbfDQnY40xsNgKHDOA7GtUrT5YVXGJfRRvU5Mxoyg1Ip7YFrZyRw8vsnkOzCtfnikAKkyLGQZJSWI+Eiq0HyxUiuanWR1qzHj0AHQjCjdoqjUDtv+ywHb3wLsuidYTR1Z13+EU40PfFeS5rmBB6fAybHkzU5VPTf2xpPEFFnUsQ3+6x1A3OFjbhIdiT16Cm+K25q8d2IHRz4j2wBSuqiLyiqG7BtyRilbyK7jqe3X6cVaEgIAYlqncnCtrpGJ+0aKPsilQcCtfBTZfiO6itAMKrQ46GtDscC2qRyKp4laohqQBQt88NIBVhcwOA7RmqdY1pRh2rgosuIKLKJW5EsqgftdvYYo5uAWgVTROwPUnxwrTbM42d24Hep619sV96qkamLmrcoyd4yC2/zwJAUWjiowUNt+0On0jFFLmREETchJyFWi5dPnipAWKPhAqAp6AeOFFLmZxs7twO9T1r7Yp96qkamLmrcoyd4yC2/zwJAUWjiowUNt+0On0jFFLmREETchJyFWi5dPnipAUxRRUUKn7Y7jCoXEqaOBxjbqoNSRgVVhjjZDIvJCvYUAPzrikAc1jemWI4A92JNDX2xVaBAIkKn9+G3XcinvhRt8VpNSziitWpFKD5Yoc09SGA40oAoHhiAniVbq9a5ZB6ZVgpWla1J279BgCZStYVeMkVq/2gcUVTUspI6At1bFSVNZZFIeu/YdcKLVkbqZFry6EHAyBXPH6T7cmJFR7YqRS0xhACKnmOvv742tLXlbiFLVbuTii1EFvtV+IdKYUL/Xl/n7V6Hritv/0IX5lRJ/+VfxVI+pEic0IMZExpUnbi2Yp2lJtjvEPNvzInH/ACsDXZLfYNdHg/Smwy7EPSEE7sekkmYMWFZDszqdyMnSCVBZaNy3G2K22ZGU9CA29MUW36jVqx6dV9saTbXrkbUC1xpeJb6w3IXc40jicJAaDj08MVtczlSPAdu+NJ5LjFVg26bV8TTFVhZaj4iB7dcUW29wxP2iy9uWKSXetuaEgeHvioLRckb7kn4vHFVRWj9MrUhv2SelMVtqSSsagJwA79vnitqXKvXbfrihosvId/GuFSrBkEvJwHSnbcA4E2pO5PT4a9cLFbU/TitrvjWhr1wJW8j0OKLbq7HffCrg3UVpgTbfPt49Tiqu0lUUFeJH7ZxTa2Vo2NVrQdAfH2xVb6pFCNq/apioLvWND3PavhitticlCruwB6AdMVtpCCQvLr0I6/TioLfH0kJpXtyHjivJaJCoFCd/tUxUFv1jQ9z2r4YrbYnJQq7sAegHTFbaQgkLy69COv04qC3x9JCaV7ch44ryaVqrU7geGNKtEhUmg+XtituWXrzqwPauKLcpTpuQT0xW1QqsfIqQSOoJ3xTyWI3w1PTrQ9K4otaZG6bUO+2K2uSd+YIA5VGKgqzLcFjQhqdABgZbqboQx5insNt8KCEemm80jaKSM8lLBagsfamAls4PNDonIFHXp4dAfDFiHBJEU05JTqa1pitLWildvhkLVGx6VxRwlZJE6N+8NSB8WG1IRdvDZSwR1uBE7H4kpuKe+AsgI1za+qzf79T+84/aXp442ij3v//RjvnWWxi83eRY1YzWPrXCyW6kCNv9IbiwboW+eYsucu+22HIPK/P0tj/j7XmdeUf1slZCafDTpx7nLYfSE7XukRltJY3SOSgrUAjiT/Zkt1sKMkdsyHgwB/m/ZHjhQQKUC61ozK3DbkB2xRbisNAar7itP1YV2XL9VelaVOwUk7D54rstaO3DMoZQOoJNcVoLeETRqQV5b1FfDAgruMRiQsOnTj4e+FaFOYwerzReK9OJPWuKTTp0ti6pFQKPtOxpXFSAt4QV2bp1GKNlrJBsQ30Yo2RMKQsnMMtelGNDgZAKdykbS0jIUAfECen04bU7rGWkYLkdaAA1IHyxRS1ooaVViR33xWgs4CnIGtN6d8UUqlUJpUliOxqD9GKgLDGpFS1KdyPwxWltErtsPfFWz6akDr44qqyCBKENXuPGnvik00zovSnxfqxRa1hE3Gmw6YpoNmOFV3Pxdh1xWg20bFVAZQhFevQ42tK1ssXpspI9QHqWoAPHFIpbcJApC8ga9eJqMUEBSVIKfaqe2Kim/TgNV5Dl4nYYrQXxG2EJLD96nQ12OKRVLVFuEZWHJjuHBxRs3IsARQD17dx88VICxUgp9qp7YqKb9OA1XkOXidhitBfEbYQksP3qdDXY4pFUtUW4RlYcmO4cHFGzpETjGqgUPSvWvvipAaAgWtRWnUjpiuzg1vWh79Wpiuy+3khUOpUOOqvTcEePtgKRTUbooZigYN9qo6HCi3OU9IBQF3qV6k++KdlrGEJTlU13YDtgpFhejW3M96fYqKb4UghoS3IJI6/qxTZaAZ1/eGik/Ee9cUc1xJUEIqbjZh1p4g4FOy8zTiJYuSop6EDcn54rZpSCzBSeYI7knCrgJ67NvTb5Yru5VejcmrT7Q69cVX82QERPRaUoQNh74E8lGi/zLha3/9KG+dJIJdU/L4SW7Q2xjM90EUkKxnJ5inbMaR3n722A2Dzv8xTZy+dNeuUohN3+6hIoePGlae+WYz6QyIHNjKtByLNGvClKDxOWMNlxghHFFjDchXmxpT6MFp4XNBbx9Yid9jXYjG1oBtY4Afscgf2ab42mgtaOP0yCgBr0p8VPbG0UKWiKJCS1GFPsEUONooOEVa8AOI6ilSPlhtNOKIFKqFYv3PUU6imBFOUcuAADDoCdj8sVadV9QqQrEde5/DCk82vq605EGg7DucBKAGqKYyxA9RSAqe2KrSgD0b4W7im2KtSUZqBO3bCELyrRpw4CrgEN1J9hipFLWUBuCjdhuPDArawgigoD15e2KQF9KVliIT0xQbbk/LFfMNKvMtyPMgcmr2wqGljjCByoZCdwDuMV2c0UQkKkinGoau3ywLW61YvH4fGtDXDa8LfpxUNSC3jitBaI+gFOQ+RrirYAKlgtXB6daYobkQuvq8ABWm3fFa6tJxKkFQvv4YEimgvwsyjl/NUYoVHROQRRyjoKnuCcU10cbdV6jl4GmG1pdEgYMFCjjuVB/rgSGqEqi0AINQvUfThRS2SjM7qgp+0BuPowK26JyCKOUdBU9wTitdHG3Veo5eBphtaXRIGDBQo47lQf64EhqhKotACDUL1H04UUuKI55rxDH9lfiHzxXhakVl4qAAf26Cik12pgVzQBD0pXficQU06MqZODMF8Kig+W2Khrdl4V5VOwH8cKFxZHNGYGQfCAo4n6TgXZzUjYcyHbsG6D22wp5NfumYuPibuuwHywI2UQJQSKtXwH8cKGiGUbuetSK4rS8x0UVI4MaivX7sCVxZV3XoNumwxVZxB2LChFakb/AEYQq0xFWp18N6YopoooBBO/amK0qBF4cgeIbbh1PzxVfRf5T0p2yNsn/9OEatVb78uWdHM13A4nBNSyiQgAeAXMWY+r3t0CKDzzzzHbjzvrEShwiXBBkYVbYdDluP6Qna0mjV5maODikfbapOSSLPJTkFaKV/edCB3phYkKbo0Z5ctj0oa4qRSoJ5eJAHBRsSRVh9OK2sUhY6VDMTsp6j6cVPJdJbqYkYAPISRQHcHwONpMWpLZoUUj4S23tv1xtBjSm0SByV6DuuxGKKWSoUqxJ5U2J7YUELmVB6fp1EfUsRRq98Cfc2XIqU5AU+KvX7sVtTLFlCp0G4xRduZyygk0r1A36YqV8JQfHIeZB+AHCodJdNJJViVNa79B8sFJMl072rSJ6SyAsP3lSN/cYqSFFGoX32G3xd6YoXBN+agAH7LNucKrELoeSNTkeJ7VGKNwvcQGQAfCg6k9/YYp2cTSQP8Aa9hsB9+K22QKEgividz8sCS6OFTuTU02xJWnFUDD4WWm/EePzxVcktsLY05rcE7uSONPDFbFOhuyFKEF0YUYHoPf2xpeJTk4hqK9IyNhTCg+TizFQqmpbv3GKi2/UPw/zKaL41wJtfWtefLme/8AXFVjBRCrNtLWilRsR3qfHFHRckdFJBbj4DCUgLX5KBGu3Lw7Yod6h+H+ZTRfGuBNr61rz5cz3/riqxgohVm2lrRSo2I71PjijouSOikgtx8BhKQG1jChitFcbj/bwLS2R6Er16EL7kVripPRcKrXkvKu5Ndx9OKrRwEDhvjFfgofsk98UdHJGpBYgdNxWlcKQF6ou47sPhA6YFroqx23qwcwQW6EnvT2wWy4bDTrCtvTkFlqfgHh44V2rzQYo0hoxofHrha+a8Inq0lqieIxTt1cJaSFwOSDptirb8Godxy3oMUldxEcRBQOex7jAg7KLcWevErXrhQvdQrgsKx7bA70xTycXrLWEEKvRT7Yrz5N/Xn8B1/HGl43/9SH+chK0/kK3YelegStZ8QVIpMaqfBa5Rkvikzx/SHmfnyS6fznrTTk+s1zSQKQF58fAdslj+kMzaTrDFGqspk9QmhKVoD2ySapp4zEK0f1CTQjf5n2xtapSNrIjBhxoDyX9qpwgo4Xc2Duzcmnbqp6Yra0PIarSjHYgCpA8MUbuVFVv2o2/kJoT7g4pqm6yFlaUswH2P7aYr722jiC1ZyGbeg3xXZZxVW4n4j1ArXFFNvEjSKUHbdT44qR3Knqwr9pTvttuBim6Q7x/EeFCK9vD54sS4xgAsDyA2Wn83hirbDpUU8D1qcKrORY0bf3A3+k9cVXgIELNRWrtQbUwJCwhQSCxHtT8cKFaNIjG7BgQpAZGryb5DAy2pQdfiYkANX7I7fdhYlyhqcm3A6A4oXLy2Vz1O1d+vtgSq0UOS6/GfsoAAp+WKWgzgkcOLdAoFae+K7t+mAQG5I3bl+18sVpr0xUFlJjB2oBUfMgYrS8xw8CWYoG/ZG+KaCnwRKdwem/XFFLpI0bgVHFq/EvbFaXAwJuyb96b74p5Kcw5V9M1U70xQfJr0xT7Xwru6jscUU6p4jkKDuRiq4orqhFedTyBqPltimrXgwJuyb96b74p5Kcw5V9M1U70xQfJr0xT7Xwru6jscUU6p4jkKDuRiq31HBI3IOxoKHCtrkC0Yk0oPhNOw8cCQtZqdXanb+zCi1SFInLNzHJRUs9RX2HvgSAFOWpYkjjXovt7++FBWoH+1uFGKFys61YniGwJbRquvIUFRWgwpUi9T0KmuLG3ASSEkHcYq2GTdVYrXrXocVtv1HVitagVC7eHhimytV3IoTXw8a4oczNWjjf2xVyqXbckAdCcVpvko+Eggjqw640i1/GD/fh+z4YLTs//9WDa7fz32r+QWDtJPZJLzkbbkDMSCo/azHyHebbDkPJ5356kaXztrUn2Xa4JKAU7b5LF9ISeaQ3FCwVZaIBVgK0rlgQW1lZuNWLCuyuAQDjSQVspug5T1CePbY0riptfGHorEqH78t3OKh1CrcwxDnqKV3xVpJZOZLIsh7M3UfLEqDbTvuYw1R+0w6fIYoPcsPKnEbtuOR3oMNopYX+JQAQw8TUk4FtsmQE1JJ8VNMUtqrAcj17htxTFAdxZQdyD1r1wqpkPyAU033B6V8cUdXB6sqkniDs2KheFJFFILE0wJVbeJXJWQjih33ocSmNdVkqkMhKV5V4EHqMUFTXkTtUEH6MKG2glWWrb03b5YE04hgP5VJ+Gu5wqvAkAJZASNlbrQ+NMCaKpEJCKsRzB2L7mn+TikAtsprzLEODuAMClr1ZOYLKstBtz7H2phW7LpJCDxBHJtzTouICCVhqBQbmvw16D3w2ilNmoApBqOrE9fl4YFbYyA1Ykk/y7YpcitXka8hvvuDigNhSCWBIr0PanyxVTbntQ0Jpy7VwoLmc14kkrXcjxxVeeQOxJHahocDJyK1eRryG++4OKA2FIJYEivQ9qfLFVNue1DQmnLtXCguZzXiSStdyPHFVSnWhq3av8cCV0MXJzG5Hid6E/LFIHe64QLRlSsYaiEGv0YhSonlUrQgjw/jhYr5IJuYYmpO4+WBNNEMASPhTxO/3YVXor7F0DqBUH5+2AqG0hZkaQAmncbUphTSiyOACykV6nrXFG67g7MQoIKipVttsbVasTSHY/Gf2cUAWrIEQhfT5SjsT1A6nAkdzRhDNUpwauzKCUp4Y2tLHhlCmqg03NOowrTZjLFA1IwfssSCuKrAgZqM1GP2T2xQF/wBTT/fqdade+Rsp4R3v/9aL+ZtINsfIF4k9sqaTFx1SRXqqsZKnfq1BmIZDf+k3RDz780bDUYfN2q6utpJHo99Pzs7koeDpxABDDxy3GRVBbPVhjMDGDyWtenemWIK5EV1YJx5f5RpUe2KaUxEQ2xq3bwOKKbi5A8CBTqD3+jFQ3KWY7/CB+yOp+eKkreRNAg4sfpxVwUrTfp4Driq9WoxUp23OKVstGmo3Ybe2KCpyABaKjDv8XXFDoxVeR/WcVXSGOiso6fZriq0VqehYnc/PFV3wgk8asDtTw9sVb+E1Kj6D1xSpheUlX79PDFFbt+mFNK18PbG1IorqEjkB8WwZfEfLFURJFbqzCGYcDTizEcie4O+BlQ6NNOs37kVB7sKbU98K8V7OYKUU0Yt0RjgSQpLyVypUUbxr19sLELpSx+H7I8e5xSSsDCg4gqegPXFDXBlBqd61JpitLw5Diqk1HXFNtT0ZkUii02GKCsdVCnijVPWuKFsQJ38Pc4qvf0jHsDt+vEKVtTUsaFv89sVXUWpqNwKinjirUlCpKo1T1riq2IE7+HucVXv6Rj2B2/XiFK2pqWNC3+e2KrqLU1G4FRTxxVv4WNQKHwPXFKmQWcFug2xQQ2Ywp6gg9B4e+NqRS7iSNvtr0r3+jFKIaK2CqY5eJZP3nMiqt4DfFIpo3AIEFKsfh5Lv9IxXi6O4oI9+TBDQN74prZbISigAEchXkf1nFBNIcy1UkliW3O+33YWC+SVpgHdAoUUUKMCSbWMSwBFKgfaAphQ3zBX+SmxI74qqrckwFFUvTckmn6sFMgdlEOWqePJj86jChd6srookPwxiiJsKYrZK3kWTjWvz7fLFFrP9l7dcVsP/1yHVvMupajrT+lP62mPWO51QqiRRuWNI/SP2flmANg5BClb+aBIt1puqh7nTGhdLqO9jRY2ZT8HpOP2OP/AtgI7uaA8m87eVbXTLyG800k6RqIZ7SpBZQOoPt4ZlY5k8+bEx7mOfZohY8Rua/wAMstaXGOVCktSRSoB2pitVup+pMGYqlK9+tB7YosterJ9sj4m2G2FbXerICSoPypjS8TkkA+KhqOhAwKC2sz8gRUGtSaV6+OKgtcmYkV71Pw4UO9R+4JPj2xW3RAtyryVf8kbVxSFRo3KqERl8agUwKpKzK+6EAe3fCgLPVYGvUe4xW13q7UAIHieuK2viKstChZ1+yf4UxSCpu1GdeBBO4BxCF4liAU8GPZu33HFNhomM8mRWAHU0riq5HgI/umNeoU0rgUEdzfrxCoVGJ6IzHpimwtaSUyclTcjbv9IxRZW+rIfjYdNgaYVtd6rgggGo6imK8TlepqwJpuSB3wLbfrOTUV5Gm9NxTGltzO7Oanc9fh/VhW92jI4NDVqdD2xRboqtIa8lB+0VGKQqcG4AIjVJ3Ygbj3wKpfGriqGgO5p1woWtK3IkfcRitrlkNOhI7YrbcVWkNeSg/aKjFIVODcAERqk7sQNx74FUvjVxVDQHc064ULWlbkSPuIxW2xLt8KnfucVtdEytVWUt3U+BxSCtkbhIf3ZUkUAOKLbWSMJuhLDr4ffithstEzExowNK1O+KbdHJCeqMT3I2rgUELvWgUkCNqDoG34nFNjuXyyQSuiRqxJotW8TiFJtSSGdB6jCiFa123w2xAK14pWiEixkAmhIPX6MVorXRlABAqdzvT78bQWhBIRWlBitNejIF5dBitK0NvIUE3McFPxb9Pn88bSB1d6UksTH4QVPT9qmK1ax4yiAMRyPUHegxtSCFnpn26V+jFX//0I5rOj6bYRyldGjnvXVp7i6kkaKilqACOoUuP9XlmvEi3lg35rXMU+uWtjb2osbOGyhk+qhjR5GH94wP7TZdgG1pq2GPeXMdi2ntJW3ZhLHE2/Fx3r+zlwG9oKFHolkp8Rr9o/zfLJIsIn9w7igpUUYHuRvtgZmihZY1ioXPIN9lAd/pG2FrIpdLGqLHRgvIVZT2OBkRTaGKOVQRXl9o9SPDFApsJCHkc7noh9/HFKJ0iKBnfmQZJEKwj36HEphXVbeaPcJJIsaBoIwA0ikElj7jEFTA9EFNDwdkgHJVA5cuzd+uEMZUOSnQlFRiVI3p29zhYqljH6l9HHXmP2VrSv303wFYjdX1S9umumh6GtAaUbbtgAZTkbpCNHJG4SQj4tzXJMeq5Wj+yxPEbAjoTgWwrwLH9pPjYmklP4dMWQQ8ihZJHO1TQb9MLEha4A4/GOI6UP68CqsaKSvLdGqTxP8AbikN8UVwyxOYjsdjUeFMV6r4oU9UVFY2Iodz9+LIB08Kx1diPTrsgNG+j2xQRTmRRAkgIQsd1PhgTWzVY42ViOROx77e2FFqhSD1yzDkqjYd98CdrVdLWD64JJiCoqFHi1NtsSmFXuqXuj3CycYUDjiXmYEErXtiCph3ICW39IqkVS/Gsit2OEMZADkpAME4NVSxrTsThYujUGeOPlVWNKVpX2PTFQN0w1i7uYpVt6cQqhRUCq/IjIgM5yIS945oypcj4/HwyQYG15jAVfR+NiCXHUChoMCTXRYAwTg1VLGtOxOFDo1Bnjj5VVjSlaV9j0xUDdMNYu7mKVbenEKoUVAqvyIyIDOciEveOaMqXI+Px8MkGBtcrRqQrE8B1IwLYVolj3ZPikXYgfy/1xZBTuFAnZyKAClK/wBuIQVJgOAo4496HeuKFSNAaGvIMaGh3p74p3XMiKwaKJmQGj7Hp7YqebYjQsGVSYz1BrUHwOKaUyGWTiGIRjuD1p7YWJa/e+l6fIcQeVO30Yr0dxfkqhwOfXsAcVc/L1PjCgrsa7g/M4oJaKyAgc/jO4HYg+GK00ouOVeVD03xTu2qniwElQepGwxVxA4qedezN2AxQudGqArckTcMor9+K7ref/Fx6/yYaV//0Y/+ZN3qMnnP1TbiW+04Q3F/esQESNj8KyRV+Jf9TNdHYOSw386tC1B/Ma+Y4Yi1tfxIXlUExh1WnweCU/4HMjDIcmFPN63I4u5Tfox7/PL0bqPOQN2LHthY23+9IMjljTZh067bYpcssK1+E1/aB3/HBS2A2bvagHNP2Q3UfThpPEtFxvugAPWmK25p61q3xdB22wUpKKtb+GJCskXrj9iQHiR88aTGdKseuOHKIqoG2WTrx96dzg4UjKo3F0l7RiG9VdpD0DH+agwgIMhJDMFjYclNCd2/28NMWpJY2b4VNQdmB/jiglMY9Stqr9cjknKLSI7clH+t3wU2CY6oS/vxcyrWIRxpsAPtU/yjhAYynaGaXqF3Q9jixtfbzBWb4uFRSgFR9GKgrPUj9Sp3UHavU4rYXGZORK/CD2AxUFVhhm4SFY1Ksu3IfiuJSArW0uqyKIU5P/ItK9qYNkx4l92NQQCOSERxqOZVNhQd64plaEEsKkkqeXgfi2wm2Nhv63QUUcoz+y29D7YKTxLfrHfgB228MNItszgn7XT7IpTBSTJWs72KGoeP6whGwrRlONJjKlddc9OUcIxw6Cp3HufGmDhT4tFZcXouwI5CzzId5BsGXsKe2EBTIHZCSIE3Kn5jt9+GmHJ0ssLVCqTt164oJRsGoQKkaXaPNCm6IaEg+zYKZiQ6qWo6ktwFjSH04V3Undz8ziAiU7Wi4gmt0gVGDR1oQaVBNaH6ca3TxAilGRAm5U/Mdvvw0x5OllhaoVSduvXFBKNg1CBUjS7R5oU3RDQkH2bBTMSHVS1HUluAsaQ+nCu6k7ufmcQESnaDaVR/d/ZPVThY2ugmCyg14U8BUYqObTyIZOpK+JG5xW22mj5ckHD2pim91W2jlLmREVlII+IfCcVAVLabUgPQjLFeioN967YNki1e4j1RI1ieAIHrypsWPep8cdmRtRM04iI9FQqgglt2p7Ysb8lEMVjKlFcP9hxWlfDFFtK1RsnbZabV98VXxybAOAqDai9K++KQWm4lwxBNNivgB0AxVcSXBZV2HvvtiqxHkWMyr8UbfC9QPhJxRvVuUyEiq1K9gaD54V3bVpONZDyjU70FP1YE2repL/Kvj0/Z8cU7v//SK/zOOifW71GFmL5mj/RM92ZTqka9/SiA9GWL/mJmh/yc18LryciSdaE2p/4baqacbVYqXJvnYWz+8PJT6ctf71U9VP8AKw7V+P8AYo6sPP8Ayrn9Nk6wdJ+qmI+urBuIm/Z9Fo+v0rxwjj6WlG3J/IY6RqQulsEm9Fvqzae0zzmXj+62kCAfF9vi2Sjx+aHjd5B5PXTma3vdVk1Kg9OGe0to7fqOXKRbmSTZa8f3XxN/LmQLYnzSH/RuR48ilN+VAa/jk12Wpwr7b0wIbi9OjfLatK1xVtfRqvLp7UwqOaqPS9FuNa706fjgSVtt6HpfAP31f2qfhTfEoFUirfjy34135cetcDOK+5EPoTeqfir8NAK0+k4hTSV0j9PYmnLfbt9+FgiLenN/S4nx5bCn0VxUeSlP6fqnx/b8PoxUqL8fU/ycQgoy35cF48PtCpPX/axLKKjOLf1j6ZJNTyqBT6KHFBrotHDlt0qKg9KYqj5OJmk58gvEcBHuv3mmAMyvhE1R6TSetxP2Aacab9DWuJQPJqH6z9UPqg8ePwlya8K9q4So5II/VuY4lj/PyoP64d0Cli+nz2+zXatMCF0fp+oa9PemKtD0tuXSu/SuFeqvF6FH419qUrXAy2U7f6vR6j97+zWlP64ljGkVD9v4gvOvxU60wBkFeT0aSeqfh4/BsK/icWRpKAI+D8ST8wB/HC17K0HH1f3VC3H9rYfhXFQ1c8ee9A/em60xUq1sdhwVD8zQ/gDiyCIlL8X5olOOwLdPlVd8CSltIvSbgzE9wVAH4E4WBqlW34er8FK8f2un4YqHT+kZf3h4mnVAGH4lcVKk4tv52G2wCg/jyGKmla348ZOJWm3Ekb/QMVi1dC39ZtyJK9AAV/XiOSZValIIuX7sk7fFUAfqJxQjXJ5RUX93w2EbbH57YGZdD9Z5R+kDzr+6oTUH3oMJYi+ipam94yeoG41enMtsf2uoxKY2pT/WPq8fOhXiadRt74qbpDS8/Ti6iL9n+Wvt74hBvbuX/sjY/wCV16e+KQuNfS7ca/u8UBQf+8PL7PM7e9e+KOq8+ny269vDFK1+XBeXSpoV/iPs4oK8f3e3GnvWv0YsguHP4v5advHCjq7954L9j3/zrkWT/9k=');
    background-attachment: fixed;
}
ul {
	list-style-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAABdElEQVR4AW1QTU7CUBAeoMXX8vp4fW1KQWmEkOBGXRtNj2BioluOwE6XHMGlS4/gEXThPTgCVxj7lUyphJd8mb/vm3kzRCeeIiq9CqFPq9zqcpan5QnaIeUcmZEz76kd7Iahz7rfYRN02UaKYxPyKLbbzJrNQdHyxon5yp3hPBmy1yW+vZ7y89MdP9xf1TFyi+KCi1G6bsmIFtPJ42We1iSQ315fakAMIIZFg0j5jLWaBgHRFoW28PfngwERigUv6NB3LZ7HcTHLkrqrEEQoYsnDnk8sx3rAsVIF4ZrzcVbviCKmixixAHkAt3BGs9W6pFSrcllMmi/LoTABe8IKEKNuwqASq5KUosKEvZqAogACTAUZvkyGr8+8/bexuPL/H0waiKAd42Ba+dvm2lHgrZFEVxAhgpWDSRPU9+Lqy+2ng/4nCoCQpRFiqSVRtGrrGj8K1UpIx7bXoV1m7U1DPuU4IpPaaG11uNHKq6A2iJ1z5pj/BzjO43C6k1UlAAAAAElFTkSuQmCC');
}
p.title {
	font-family:'Times New Roman';
	color: #b08663;
	font-size: 16px;
	margin: 0px;
}

a.description {
	font-family:'Times New Roman';
	color: #a7b6cf;
	font-size: 13px;
	margin: 0px;
}

p.date {
	font-size: 12px;
	font-family:'Arial';
	margin: 0px;
	color: #999999;
}

li {
	margin-bottom: 10px;
}
</STYLE>")
        f.WriteLine("<BODY><UL>")
        f.Close()
        ' Adding news content
        newsParse()
        ' newsParse has finished. Lets now load the URL and make our webbrowser visible
        CheckIfScrollReq()
        Console.WriteLine("Navigating to: " & "file:///" & Path.GetTempPath & "news\index.html")
        WebBrowser1.Navigate("file:///" & Path.GetTempPath & "news\index.html")
        ' Do we need scroll bar?

    End Sub
    Sub newsParse()
        For v = 0 To result.Length - 1
            ' Title
            Dim title As String = result(v).Title()
            ' Date
            Dim [Date] As String = result(v).Date()
            ' Description
            Dim description As String = result(v).Description()
            ' URL

            Dim URL As String = result(v).Link()
            'Dim URLParsed As String = URL.Insert(5, ":")
            'Console.WriteLine(URLParsed)
            Console.WriteLine(URL)
            createNewLists(title, [Date], description, URL)
        Next
        Dim f As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(Path.GetTempPath & "news\index.html", True)
        f.WriteLine("</UL>")
        f.WriteLine("</BODY>")
        f.WriteLine("</HTML>")
        f.Close()
    End Sub
    Sub createNewLists(ByVal title As String, ByVal [Date] As String, ByVal description As String, ByVal URL As String)
        Dim f As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter(Path.GetTempPath & "news\index.html", True)
        f.WriteLine("<li>
		<b>
		<p class='title'>{0}</p>
		<p class='date'>{1}</p>
		<a class='description' href='{2}'>{3}</a>
		</b>
	</li>", title, [Date], URL, description)
        f.Close()
    End Sub
    Function IfNewsExists()
        ' We want to know if our directory "news" is there, and that we have an index.html
        If File.Exists(Path.GetTempPath & "news\index.html") = True Then
            ' All the files exist
            Console.WriteLine("News exists")
            Return True
        Else
            ' News isnt there
            Console.WriteLine("News does not exist")
            Return False
        End If
    End Function
    Sub CheckIfScrollReq()
        Dim fileReader As String
        fileReader = My.Computer.FileSystem.ReadAllText(Path.GetTempPath & "news\index.html")
        ' Figure out how many URL's are in there.
        Dim searchTerm As String = "description"
        ' Convert the string into an array of words.
        Dim dataSource As String() = fileReader.Split(New Char() {"'"},
                                                 StringSplitOptions.RemoveEmptyEntries)
        ' Create and execute the query. It executes immediately 
        ' because a singleton value is produced.
        Dim matchQuery = From word In dataSource
                         Where word.ToLowerInvariant() = searchTerm.ToLowerInvariant()
                         Select word

        ' Count the matches.
        Dim count As Integer = matchQuery.Count()
        ' If there are more than 6 urls, enable the scroll bar to show other news
        If count >= 5 Then
            WebBrowser1.ScrollBarsEnabled = True
            Console.WriteLine("Scrollbars have been enabled!")
        Else
            Console.WriteLine("Scrollbars are not enabled!")
        End If
    End Sub
    Private Sub WebBrowser1_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted
        ' Now that the news has loaded, lets change the visibility to true so the user can see
        Console.WriteLine("Web Browser is now visible")
        WebBrowser1.Visible = True
    End Sub
    ' Opens web browser instead of opening link inside of WebBrowser1
    Private Sub WebBrowser1_Navigating(sender As Object, e As WebBrowserNavigatingEventArgs) Handles WebBrowser1.Navigating
        ' Make sure it isn't our main news page, we want that loaded inside WebBrowser1
        If Not (e.Url.ToString().EndsWith("news/index.html")) Then
            Process.Start(e.Url.ToString())
            e.Cancel = True
        End If
    End Sub
    Private Sub Launcher_LostFocus(sender As Object, e As EventArgs) Handles Me.LostFocus
        txtUser.Select()
    End Sub
#End Region
End Class
Public Class RootObject
    Public Property id() As String
        Get
            Return m_id
        End Get
        Set
            m_id = Value
        End Set
    End Property
    Private m_id As String
    Public Property Title() As String
        Get
            Return m_Title
        End Get
        Set
            m_Title = Value
        End Set
    End Property
    Private m_Title As String
    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set
            m_Description = Value
        End Set
    End Property
    Private m_Description As String
    Public Property Link() As String
        Get
            Return m_Link
        End Get
        Set
            m_Link = Value
        End Set
    End Property
    Private m_Link As String
    Public Property [Date]() As String
        Get
            Return m_Date
        End Get
        Set
            m_Date = Value
        End Set
    End Property
    Private m_Date As String
End Class

