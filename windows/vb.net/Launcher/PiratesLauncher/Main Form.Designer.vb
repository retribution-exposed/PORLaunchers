﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Launcher
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Launcher))
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.btnMinimize = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnPlay = New System.Windows.Forms.Button()
        Me.btnForgot = New System.Windows.Forms.Button()
        Me.btnCreateAccount = New System.Windows.Forms.Button()
        Me.btnOfficialSite = New System.Windows.Forms.Button()
        Me.btnCommunity = New System.Windows.Forms.Button()
        Me.btnPlayersGuide = New System.Windows.Forms.Button()
        Me.btnReleaseNotes = New System.Windows.Forms.Button()
        Me.btnManageAccount = New System.Windows.Forms.Button()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.Updater = New System.ComponentModel.BackgroundWorker()
        Me.btnManageAccDefault = New System.Windows.Forms.Button()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.lblUpdate = New System.Windows.Forms.Label()
        Me.ColorProgressBar1 = New ProgressBars.ColourProgressBar()
        Me.cbSaveLogin = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'txtUser
        '
        Me.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUser.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtUser.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser.Location = New System.Drawing.Point(505, 200)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(157, 20)
        Me.txtUser.TabIndex = 0
        '
        'txtPass
        '
        Me.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPass.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPass.Location = New System.Drawing.Point(505, 228)
        Me.txtPass.Name = "txtPass"
        Me.txtPass.Size = New System.Drawing.Size(157, 20)
        Me.txtPass.TabIndex = 1
        Me.txtPass.UseSystemPasswordChar = True
        '
        'btnMinimize
        '
        Me.btnMinimize.BackColor = System.Drawing.Color.Transparent
        Me.btnMinimize.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.TMIN1U
        Me.btnMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnMinimize.FlatAppearance.BorderSize = 0
        Me.btnMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinimize.ForeColor = System.Drawing.Color.Transparent
        Me.btnMinimize.Location = New System.Drawing.Point(757, 20)
        Me.btnMinimize.Margin = New System.Windows.Forms.Padding(0)
        Me.btnMinimize.Name = "btnMinimize"
        Me.btnMinimize.Size = New System.Drawing.Size(18, 18)
        Me.btnMinimize.TabIndex = 4
        Me.btnMinimize.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.TQUIT1U
        Me.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnExit.FlatAppearance.BorderSize = 0
        Me.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.ForeColor = System.Drawing.Color.Transparent
        Me.btnExit.Location = New System.Drawing.Point(775, 20)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(18, 18)
        Me.btnExit.TabIndex = 5
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnPlay
        '
        Me.btnPlay.BackColor = System.Drawing.Color.Transparent
        Me.btnPlay.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.PLAY1D
        Me.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnPlay.FlatAppearance.BorderSize = 0
        Me.btnPlay.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnPlay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPlay.ForeColor = System.Drawing.Color.Transparent
        Me.btnPlay.Location = New System.Drawing.Point(668, 200)
        Me.btnPlay.Margin = New System.Windows.Forms.Padding(0)
        Me.btnPlay.Name = "btnPlay"
        Me.btnPlay.Size = New System.Drawing.Size(83, 31)
        Me.btnPlay.TabIndex = 6
        Me.btnPlay.UseVisualStyleBackColor = False
        '
        'btnForgot
        '
        Me.btnForgot.BackColor = System.Drawing.Color.Transparent
        Me.btnForgot.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.FORGOT1U
        Me.btnForgot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnForgot.FlatAppearance.BorderSize = 0
        Me.btnForgot.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnForgot.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnForgot.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnForgot.ForeColor = System.Drawing.Color.Transparent
        Me.btnForgot.Location = New System.Drawing.Point(414, 255)
        Me.btnForgot.Margin = New System.Windows.Forms.Padding(0)
        Me.btnForgot.Name = "btnForgot"
        Me.btnForgot.Size = New System.Drawing.Size(103, 15)
        Me.btnForgot.TabIndex = 8
        Me.btnForgot.UseVisualStyleBackColor = False
        '
        'btnCreateAccount
        '
        Me.btnCreateAccount.BackColor = System.Drawing.Color.Transparent
        Me.btnCreateAccount.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.CREATEPW1U
        Me.btnCreateAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnCreateAccount.FlatAppearance.BorderSize = 0
        Me.btnCreateAccount.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnCreateAccount.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnCreateAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCreateAccount.ForeColor = System.Drawing.Color.Transparent
        Me.btnCreateAccount.Location = New System.Drawing.Point(624, 255)
        Me.btnCreateAccount.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCreateAccount.Name = "btnCreateAccount"
        Me.btnCreateAccount.Size = New System.Drawing.Size(92, 15)
        Me.btnCreateAccount.TabIndex = 11
        Me.btnCreateAccount.UseVisualStyleBackColor = False
        '
        'btnOfficialSite
        '
        Me.btnOfficialSite.BackColor = System.Drawing.Color.Transparent
        Me.btnOfficialSite.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.WEBSITE1D
        Me.btnOfficialSite.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnOfficialSite.FlatAppearance.BorderSize = 0
        Me.btnOfficialSite.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnOfficialSite.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnOfficialSite.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOfficialSite.ForeColor = System.Drawing.Color.Transparent
        Me.btnOfficialSite.Location = New System.Drawing.Point(36, 505)
        Me.btnOfficialSite.Margin = New System.Windows.Forms.Padding(0)
        Me.btnOfficialSite.Name = "btnOfficialSite"
        Me.btnOfficialSite.Size = New System.Drawing.Size(117, 23)
        Me.btnOfficialSite.TabIndex = 12
        Me.btnOfficialSite.UseVisualStyleBackColor = False
        '
        'btnCommunity
        '
        Me.btnCommunity.BackColor = System.Drawing.Color.Transparent
        Me.btnCommunity.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.COMMUNITY1D
        Me.btnCommunity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnCommunity.FlatAppearance.BorderSize = 0
        Me.btnCommunity.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnCommunity.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnCommunity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCommunity.ForeColor = System.Drawing.Color.Transparent
        Me.btnCommunity.Location = New System.Drawing.Point(158, 505)
        Me.btnCommunity.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCommunity.Name = "btnCommunity"
        Me.btnCommunity.Size = New System.Drawing.Size(117, 23)
        Me.btnCommunity.TabIndex = 13
        Me.btnCommunity.UseVisualStyleBackColor = False
        '
        'btnPlayersGuide
        '
        Me.btnPlayersGuide.BackColor = System.Drawing.Color.Transparent
        Me.btnPlayersGuide.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.FAQ1D
        Me.btnPlayersGuide.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnPlayersGuide.FlatAppearance.BorderSize = 0
        Me.btnPlayersGuide.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnPlayersGuide.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnPlayersGuide.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPlayersGuide.ForeColor = System.Drawing.Color.Transparent
        Me.btnPlayersGuide.Location = New System.Drawing.Point(279, 505)
        Me.btnPlayersGuide.Margin = New System.Windows.Forms.Padding(0)
        Me.btnPlayersGuide.Name = "btnPlayersGuide"
        Me.btnPlayersGuide.Size = New System.Drawing.Size(117, 23)
        Me.btnPlayersGuide.TabIndex = 14
        Me.btnPlayersGuide.UseVisualStyleBackColor = False
        '
        'btnReleaseNotes
        '
        Me.btnReleaseNotes.BackColor = System.Drawing.Color.Transparent
        Me.btnReleaseNotes.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.HOMEPAGE1D
        Me.btnReleaseNotes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnReleaseNotes.FlatAppearance.BorderSize = 0
        Me.btnReleaseNotes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnReleaseNotes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnReleaseNotes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReleaseNotes.ForeColor = System.Drawing.Color.Transparent
        Me.btnReleaseNotes.Location = New System.Drawing.Point(399, 505)
        Me.btnReleaseNotes.Margin = New System.Windows.Forms.Padding(0)
        Me.btnReleaseNotes.Name = "btnReleaseNotes"
        Me.btnReleaseNotes.Size = New System.Drawing.Size(117, 23)
        Me.btnReleaseNotes.TabIndex = 15
        Me.btnReleaseNotes.UseVisualStyleBackColor = False
        '
        'btnManageAccount
        '
        Me.btnManageAccount.BackColor = System.Drawing.Color.Transparent
        Me.btnManageAccount.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.MANAGEACCOUNT1D
        Me.btnManageAccount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnManageAccount.FlatAppearance.BorderSize = 0
        Me.btnManageAccount.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnManageAccount.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnManageAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManageAccount.ForeColor = System.Drawing.Color.Transparent
        Me.btnManageAccount.Location = New System.Drawing.Point(519, 505)
        Me.btnManageAccount.Margin = New System.Windows.Forms.Padding(0)
        Me.btnManageAccount.Name = "btnManageAccount"
        Me.btnManageAccount.Size = New System.Drawing.Size(117, 23)
        Me.btnManageAccount.TabIndex = 16
        Me.btnManageAccount.UseVisualStyleBackColor = False
        '
        'btnHelp
        '
        Me.btnHelp.BackColor = System.Drawing.Color.Transparent
        Me.btnHelp.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.REPORTBUG1D
        Me.btnHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnHelp.FlatAppearance.BorderSize = 0
        Me.btnHelp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnHelp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHelp.ForeColor = System.Drawing.Color.Transparent
        Me.btnHelp.Location = New System.Drawing.Point(643, 505)
        Me.btnHelp.Margin = New System.Windows.Forms.Padding(0)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(117, 23)
        Me.btnHelp.TabIndex = 17
        Me.btnHelp.UseVisualStyleBackColor = False
        '
        'Updater
        '
        Me.Updater.WorkerReportsProgress = True
        '
        'btnManageAccDefault
        '
        Me.btnManageAccDefault.BackColor = System.Drawing.Color.Transparent
        Me.btnManageAccDefault.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.manage_default
        Me.btnManageAccDefault.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnManageAccDefault.FlatAppearance.BorderSize = 0
        Me.btnManageAccDefault.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btnManageAccDefault.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnManageAccDefault.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnManageAccDefault.ForeColor = System.Drawing.Color.Transparent
        Me.btnManageAccDefault.Location = New System.Drawing.Point(523, 255)
        Me.btnManageAccDefault.Margin = New System.Windows.Forms.Padding(0)
        Me.btnManageAccDefault.Name = "btnManageAccDefault"
        Me.btnManageAccDefault.Size = New System.Drawing.Size(92, 15)
        Me.btnManageAccDefault.TabIndex = 18
        Me.btnManageAccDefault.UseVisualStyleBackColor = False
        '
        'WebBrowser1
        '
        Me.WebBrowser1.AllowWebBrowserDrop = False
        Me.WebBrowser1.IsWebBrowserContextMenuEnabled = False
        Me.WebBrowser1.Location = New System.Drawing.Point(31, 132)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.ScriptErrorsSuppressed = True
        Me.WebBrowser1.ScrollBarsEnabled = False
        Me.WebBrowser1.Size = New System.Drawing.Size(336, 326)
        Me.WebBrowser1.TabIndex = 20
        Me.WebBrowser1.Visible = False
        '
        'lblUpdate
        '
        Me.lblUpdate.BackColor = System.Drawing.Color.Transparent
        Me.lblUpdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpdate.ForeColor = System.Drawing.Color.White
        Me.lblUpdate.Location = New System.Drawing.Point(445, 300)
        Me.lblUpdate.Name = "lblUpdate"
        Me.lblUpdate.Size = New System.Drawing.Size(280, 13)
        Me.lblUpdate.TabIndex = 22
        Me.lblUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ColorProgressBar1
        '
        Me.ColorProgressBar1.BackColor = System.Drawing.Color.Transparent
        Me.ColorProgressBar1.BarColor = System.Drawing.SystemColors.Highlight
        Me.ColorProgressBar1.BorderColor = System.Drawing.Color.Transparent
        Me.ColorProgressBar1.EmptyColor = System.Drawing.Color.Transparent
        Me.ColorProgressBar1.Location = New System.Drawing.Point(451, 275)
        Me.ColorProgressBar1.Name = "ColorProgressBar1"
        Me.ColorProgressBar1.ProgressMaximumValue = 100
        Me.ColorProgressBar1.ShowBorder = False
        Me.ColorProgressBar1.Size = New System.Drawing.Size(270, 14)
        Me.ColorProgressBar1.TabIndex = 23
        Me.ColorProgressBar1.Value = 0
        '
        'cbSaveLogin
        '
        Me.cbSaveLogin.AutoSize = True
        Me.cbSaveLogin.BackColor = System.Drawing.Color.Transparent
        Me.cbSaveLogin.ForeColor = System.Drawing.Color.White
        Me.cbSaveLogin.Location = New System.Drawing.Point(668, 235)
        Me.cbSaveLogin.Name = "cbSaveLogin"
        Me.cbSaveLogin.Size = New System.Drawing.Size(80, 17)
        Me.cbSaveLogin.TabIndex = 24
        Me.cbSaveLogin.Text = "Save Login"
        Me.cbSaveLogin.UseVisualStyleBackColor = False
        '
        'Launcher
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.BackgroundImage = Global.PiratesLauncher.My.Resources.Resources.mainbg
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(806, 560)
        Me.Controls.Add(Me.cbSaveLogin)
        Me.Controls.Add(Me.ColorProgressBar1)
        Me.Controls.Add(Me.lblUpdate)
        Me.Controls.Add(Me.WebBrowser1)
        Me.Controls.Add(Me.btnManageAccDefault)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnManageAccount)
        Me.Controls.Add(Me.btnReleaseNotes)
        Me.Controls.Add(Me.btnPlayersGuide)
        Me.Controls.Add(Me.btnCommunity)
        Me.Controls.Add(Me.btnOfficialSite)
        Me.Controls.Add(Me.btnCreateAccount)
        Me.Controls.Add(Me.btnForgot)
        Me.Controls.Add(Me.btnPlay)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnMinimize)
        Me.Controls.Add(Me.txtPass)
        Me.Controls.Add(Me.txtUser)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximumSize = New System.Drawing.Size(806, 560)
        Me.MinimumSize = New System.Drawing.Size(806, 560)
        Me.Name = "Launcher"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pirates Online Retribution - Launcher"
        Me.TransparencyKey = System.Drawing.Color.FromArgb(CType(CType(72, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(23, Byte), Integer))
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtUser As TextBox
    Friend WithEvents txtPass As TextBox
    Friend WithEvents btnMinimize As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnPlay As Button
    Friend WithEvents btnForgot As Button
    Friend WithEvents btnCreateAccount As Button
    Friend WithEvents btnOfficialSite As Button
    Friend WithEvents btnCommunity As Button
    Friend WithEvents btnPlayersGuide As Button
    Friend WithEvents btnReleaseNotes As Button
    Friend WithEvents btnManageAccount As Button
    Friend WithEvents btnHelp As Button
    Friend WithEvents Updater As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnManageAccDefault As Button
    Friend WithEvents WebBrowser1 As WebBrowser
    Friend WithEvents lblUpdate As Label
    Friend WithEvents ColorProgressBar1 As ProgressBars.ColourProgressBar
    Friend WithEvents cbSaveLogin As CheckBox
End Class
